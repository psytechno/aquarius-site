const HtmlWebPackPlugin = require('html-webpack-plugin');
let FaviconsWebpackPlugin = require('favicons-webpack-plugin');
var webpack = require('webpack');
module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: 'html-loader'
          }
        ]
      },
      { test: /(\.css$)/, loaders: ['style-loader', 'css-loader'] },
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' }
    ]
  },
  plugins: [
    new FaviconsWebpackPlugin('./static/water-solid.png'),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
      'process.env.SITE_HOST': JSON.stringify(process.env.SITE_HOST),
      'process.env.API_HOST': JSON.stringify(process.env.API_HOST),
      'process.env.API_PREFIX': JSON.stringify(process.env.API_PREFIX),
      'process.env.WS_HOST': JSON.stringify(process.env.WS_HOST),
    }),
    new HtmlWebPackPlugin({
      template: './src/index.html',
      filename: './index.html'
    })
  ],
  output: {
    publicPath: '/'
  }
};
