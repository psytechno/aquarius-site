import React from 'react';
import { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from '../../redux/store';

import Sidebar from './sidebar';
import Topbar from './topbar';
import ContainerFluid from './container-fluid';
import Footer from './footer';
import Modal from './modal';
import Login from './login';
import { BrowserRouter as Router } from "react-router-dom";

import '../../../css/sb-admin-2.min.css';
import '../../../css/fontawesome-free/css/all.min.css';
import '../../../css/tweaks.css';

import { hideModal, setSocket } from '../../redux/reducers/ui';
import { userMe } from '../../redux/reducers/user';

import { connect } from 'react-redux';

import { getSocket } from '../../stuff/websocket';



const mapStateToProps = state => ({
  ui: state.ui,
  user: state.user,
});

const mapDispatchToProps = { hideModal, userMe, setSocket };

store.subscribe(() => {
  const user = { ...store.getState().user };
  localStorage.setItem('reduxState', JSON.stringify({ user }))
});

/*let uploadUrl = '';
if (process.env.API_HOST)
  uploadUrl = uploadUrl + process.env.API_HOST;
if (process.env.API_PREFIX)
  uploadUrl = uploadUrl + process.env.API_PREFIX;
uploadUrl = uploadUrl + '/upload';*/

const headerText = ui => {
  if (!ui)
    return '';
  switch (ui.loginFormMode) {
    case 'login': {
      return 'Login';
    }
    case 'register': {
      return 'Create new account';
    }
    case 'reset': {
      return 'Reset password';
    }
    default: {
      return '';
    }
  }
}

class Layout extends Component {
  constructor() {
    super();
  }
  componentWillMount() {
    const socket = getSocket();
    this.props.setSocket(socket);
    
    socket.on('hello', console.log);
  }
  componentDidMount() {
    if (this.props.user.id)
      this.props.userMe();    
  }
  render() {
    return (
      <Router>
        <Sidebar />
        <div id="content-wrapper" className="d-flex flex-column">
          <div id="content">
            <Topbar />
            <ContainerFluid />
          </div>
          <Footer />
        </div>
        <Modal name="Authorization" headerText={headerText(this.props.ui)}><Login done={() => this.props.hideModal("Authorization")} /></Modal>
      </Router>
    )
  }
}

Layout = connect(mapStateToProps, mapDispatchToProps)(Layout);

export default Layout;

const wrapper = document.getElementById('wrapper');
wrapper
  ? ReactDOM.render(
    <Provider store={store}>
      <Layout />
    </Provider>,
    wrapper
  )
  : false;