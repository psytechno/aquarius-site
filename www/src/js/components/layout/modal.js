import React from 'react';
import { connect } from 'react-redux';
import { hideModal } from '../../redux/reducers/ui';

const mapStateToProps = function (state) {
  return {
    modal: state.ui.modal
  }
}

const mapDispatchToProps = { hideModal };

class Modal extends React.Component {
  constructor() {
    super();
  }
  render() {

    const { show } = this.props.modal[this.props.name] || { show: false };

    if (!show) return null;

    return (
      <div className={"modal fade" + (show ? " show" : "")} id="logoutModal" tabIndex="-1" role="dialog"
        aria-labelledby="exampleModalLabel" style={{ display: (show ? "block" : "none") }} aria-hidden={!show} aria-modal={show}>
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header bg-primary text-white">
              <h5 className="modal-title" id="exampleModalLabel">{this.props.headerText || this.props.name}</h5>
              <button className="close text-white" type="button" aria-label="Close" onClick={()=>this.props.hideModal(this.props.name)}>
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">{this.props.children}</div>            
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Modal);