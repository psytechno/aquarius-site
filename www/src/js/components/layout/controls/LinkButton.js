import React from 'react';
import { Link } from 'react-router-dom';

const LinkButton = (props) => {
    const buttonColor = props.buttonColor || "primary";
    if (!["primary", "success", "info", "warning", "danger", "secondary", "light"].includes(buttonColor)) {
        console.error("Wrong value for button color");
        return null;
    }
    
    return (
        <Link to={props.to} className={"btn btn-" + buttonColor + (props.buttonSplit ? " btn-icon-split" : "")}>
            {props.buttonSplit ? <span class="icon text-white-50">
                <i className={props.iClassName}></i>
            </span> : null}
            <span className="text">{props.text}</span>
        </Link>
    );
};

export default LinkButton;