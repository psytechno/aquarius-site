import docs from './docs';
export const Docs = docs;

import dashboard from './dashboard';
export const Dashboard = dashboard;

import projects from './projects';
export const Projects = projects;