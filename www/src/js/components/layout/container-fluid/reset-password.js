
import React from 'react';
import { connect } from 'react-redux';
import { userResetPassword } from '../../../redux/reducers/user';
import { withRouter } from 'react-router';
import ChangePassword from './profile/change-password';

const mapStateToProps = function (state) {
  return {
    ui: state.ui
  }
};

const mapDispatchToProps = { userResetPassword };

class ResetPassword extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (<React.Fragment>
      <div className="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 className="h3 mb-0 text-gray-800">Password reset</h1>
      </div>
      <ChangePassword code={this.props.match.params.code} />
    </React.Fragment>);
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ResetPassword));