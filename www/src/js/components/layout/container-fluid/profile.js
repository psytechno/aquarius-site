import React from 'react';
import { connect } from 'react-redux';
import { userClearError, userClearResult, userMe } from '../../../redux/reducers/user';
import UserData from './profile/user-data';
import ChangePassword from './profile/change-password';

const mapStateToProps = function (state) {
  return {
    user: state.user
  }
};

const mapDispatchToProps = { userClearError, userClearResult, userMe };

class Profile extends React.Component {
  constructor() {
    super();
  }
  componentDidMount() {
    
  }
  componentDidUpdate(prevProps) {
    if (!prevProps.user.result && this.props.user.result) {
      this.props.userMe();
    }
  }
  render() {

    return (
      <React.Fragment>
        <UserData />
        <ChangePassword />
      </React.Fragment>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);