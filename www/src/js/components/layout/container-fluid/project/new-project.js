import React from 'react';
import { connect } from 'react-redux';
import { createProject, clearProjectResult, clearProjectError, createPending } from '../../../../redux/reducers/project';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { getErrorMessage } from '../../../../stuff';
import { withRouter } from "react-router-dom";

const formName = 'newProject';

const selector = formValueSelector(formName);

const mapStateToProps = function (state) {
  return {
    project: state.project,
    name: selector(state, 'name')
  }
};

const mapDispatchToProps = { createProject, clearProjectResult, clearProjectError };

class NewProject extends React.Component {
  constructor() {
    super();
  }
  getErrorMessage() {
    return getErrorMessage(this.props.project);
  }
  componentDidMount() {
    this.props.clearProjectResult();
    this.props.clearProjectError();
  }
  componentDidUpdate(prevProps) {
    const result = this.props.project.result;
    if (result && result.identifiers && Array.isArray(result.identifiers) && result.identifiers.length > 0 && typeof result.identifiers[0].id == 'number' && !prevProps.result) {
      this.props.history.push("/project");
    }
  }
  render() {
    const onSubmit = e => {
      e.preventDefault();
      this.props.createProject({ name: this.props.name });
    }

    const message = this.getErrorMessage();

    return (<React.Fragment>
      <form name="form-register" className="user new-project" onSubmit={onSubmit}>
        <div className="form-group">
          <h4>New project</h4>
        </div>
        <div className="form-group">
          <Field autoComplete="off" name="name" component="input" type="text" className="form-control" id="exampleInputProjectName" aria-describedby="projectNameHelp" placeholder="Project name..." required />
        </div>
        {message &&
          <div className="form-group alert alert-danger">
            {message}
          </div>}
        <div className="form-group">
          <button className="btn btn-success btn-icon-split" type="submit">
            <span className="icon text-white-50">
              {createPending(this.props.project) ?
                <div className="spinner-border" role="status">
                  <span className="sr-only">Loading...</span>
                </div> :
                <i className="fas fa-check"></i>}
            </span>
            <span className="text">Create project</span>
          </button>
          <button className="btn btn-secondary btn-icon-split" type="reset" onClick={() => this.props.history.push("/project")} style={{ float: "right" }}>
            <span className="icon text-white-50">
              <i className="fas fa-times"></i>
            </span>
            <span className="text">Cancel</span>
          </button>
        </div>
      </form>
    </React.Fragment>);
  }
}

export default withRouter(reduxForm({ form: formName })(connect(mapStateToProps, mapDispatchToProps)(NewProject)));