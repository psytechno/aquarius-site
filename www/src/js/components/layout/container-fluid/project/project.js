import React from 'react';
import { connect } from 'react-redux';
import { getProject, setCurrentProject, deleteProject, clearProjectResult, clearProjectError, ifExist, existPending, runProgram, setProjectProperty } from '../../../../redux/reducers/project';
import { showModal, hideModal } from '../../../../redux/reducers/ui';
import { Link } from 'react-router-dom';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import Modal from '../../modal';
import Dialog from '../../dialog';
import FileUploadProgress from 'react-fileupload-progress';
import { getAuthorizationHeader, apiUrl } from '../../../../stuff';


const formName = 'editProject';

const selector = formValueSelector(formName);

const mapStateToProps = function (state) {
  return {
    ui: state.ui,
    user: state.user,
    project: state.project
  }
};

const mapDispatchToProps = { ifExist, showModal, getProject, setCurrentProject, clearProjectResult, clearProjectError, runProgram, setProjectProperty };

class Project extends React.Component {
  constructor() {
    super();
    this.renderProjectCard = this.renderProjectCard.bind(this);
    this.renderInputBlock = this.renderInputBlock.bind(this);
    this.renderOutputBlock = this.renderOutputBlock.bind(this);
  }
  componentDidUpdate(prevProps) {
    if (this.props.ui.socket && this.props.project.current && this.props.project.current.tempId
      && (!prevProps.project.current || !prevProps.project.current.tempId)) {
      this.props.ui.socket.emit('project', {
        tag: 'init',
        id: this.props.match.params.id,
        tempId: this.props.project.current.tempId
      });

      this.props.ui.socket.on('setProjectProperty', data => {
        if (data.id != this.props.match.params.id)
          return;
        const p = this.props.project.fromGateway && this.props.project.fromGateway[data.id];
        //console.log(this, data.id, data, p);
        if (p && p.unzip && data.unzip === false) {
          this.props.ifExist(this.props.match.params.id, 'inp');
        }
        if (p && p.zipOut && data.zipOut === false) {
          this.props.ifExist(this.props.match.params.id, 'OUT.zip');
        }
        this.props.setProjectProperty(data);
      });
    }
    /*if (!prevProps.project.result && this.props.project.result) {
      this.props.getProject(this.props.match.params.id);
    }*/
  }
  componentWillMount() {

  }
  componentDidMount() {
    this.props.getProject(this.props.match.params.id);
    this.props.ifExist(this.props.match.params.id, 'inp');
    this.props.ifExist(this.props.match.params.id, 'OUT.zip');


  }
  renderProjectCard(project, divClass = 'col-xl-12 col-md-12 mb-4', renderDelete = true) {
    let d = new Date(project.insertDate);
    const deleteProjectClick = id => {
      this.props.clearProjectResult();
      this.props.clearProjectError();
      this.props.setCurrentProject({ id });
      this.props.showModal("DeleteProjectConfirmation");
    }
    return (<div className={divClass}>
      <div className="card h-100 py-2">
        <div className="card-body">
          <div className="row no-gutters align-items-center">
            <div className="col mr-2">
              <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">{d.toLocaleString()}</div>
              <div className="h5 mb-0 font-weight-bold text-gray-800">{project.name}</div>
            </div>
            {renderDelete &&
              <div className="col-auto">
                <a href="#" onClick={() => deleteProjectClick(project.id)}><i className="fas fa-trash fa-2x text-gray-300"></i></a>
              </div>}
          </div>
        </div>
      </div>
    </div>);
  }
  renderInputBlock(project, divClass = 'col-xl-12 col-md-12 mb-4') {
    const uploadUrl = apiUrl + '/upload/project/' + project.id;

    const getMessage = () => {
      const p = this.props.project.fromGateway && this.props.project.fromGateway[this.props.match.params.id];

      if (!p)
        return null;

      if (p.pendingUnmount)
        return 'Unmounting filesystem...';

      if (p.unzip)
        return 'Unpacking archive...';

      return null;
    }

    const message = getMessage();

    return (<div className={divClass}>
      <div className="card h-100 py-2">
        <div className="card-body">
          <div className="row no-gutters align-items-center">
            <div className="col mr-2">
              <div className="text-xs font-weight-bold text-primary mb-1">Input (use zip archive with 'inp' folder inside)</div>
            </div>
          </div>
          <div className="row no-gutters align-items-center">
            <div className="col-auto">
              <FileUploadProgress key='ex1' url={uploadUrl}
                onProgress={(e, request, progress) => {
                  //console.log('progress', e, request, progress); 
                }}
                onLoad={(e, request) => {
                  //console.log('load', e, request);
                }}
                onError={(e, request) => {
                  //console.log('error', e, request); 
                }}
                onAbort={(e, request) => {
                  //console.log('abort', e, request); 
                }}
                beforeSend={request => {
                  request.setRequestHeader('Authorization', getAuthorizationHeader(this.props));
                  return request;
                }}
              />
            </div>
            {message && <div className="col-auto">
              <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
              </div>
              <div>{message}</div>
            </div>}
          </div>
        </div>
      </div>
    </div>);
  }
  renderOutputBlock(project, divClass = 'col-xl-12 col-md-12 mb-4') {
    const outUrl = `${process.env.API_HOST}${process.env.API_PREFIX}/project/download/${project.exist['OUT.zip']}`;
    const runProgram = e => {
      e.preventDefault();
      this.props.runProgram(project.id);
    }
    const getMessage = () => {
      const p = this.props.project.fromGateway && this.props.project.fromGateway[this.props.match.params.id];

      if (!p)
        return null;

      if (p.runProgram)
        return 'Program is running...';

      if (p.zipOut)
        return 'Preparing output archive...';

      return null;
    }

    const message = getMessage();
    return (<div className={divClass}>
      <div className="card h-100 py-2">
        <div className="card-body">
          <div className="row no-gutters align-items-center">
            <div className={divClass}>
              <form target="_blank" method="get" action={outUrl}>
                <button className="btn btn-success btn-icon-split" onClick={runProgram}>
                  <span className="icon text-white-50">
                    <i className="fas fa-play"></i>
                  </span>
                  <span className="text">Run program...</span>
                </button>
                {existPending(this.props.project, null, 'OUT.zip') ?
                  <div className="spinner-border" role="status" style={{ float: 'right' }}>
                    <span className="sr-only">Loading...</span>
                  </div> :
                  null}
                {!message && project.exist && project.exist['OUT.zip'] ?

                  <button target="blank" className="btn btn-success btn-icon-split" type="submit" style={{ float: 'right' }}>
                    <span className="icon text-white-50">
                      <i className="fas fa-download"></i>
                    </span>
                    <span className="text">OUT.zip</span>
                  </button>

                  :
                  null}
              </form>
            </div>
          </div>
          <div className="row no-gutters align-items-center">
            {message && <div className="col-auto">
              <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
              </div>
              <div>{message}</div>
            </div>}

          </div>
        </div>
      </div>
    </div >);
  }
  render() {

    const currentProject = this.props.project.current;

    if (!currentProject)
      return null;

    const onConfirmDelete = [deleteProject(currentProject.id), hideModal("DeleteProjectConfirmation")];
    const onCancelDelete = [hideModal("DeleteProjectConfirmation")];

    const deleteProjectButtons = [
      {
        text: 'Delete',
        actions: onConfirmDelete,
        float: 'left',
        color: 'danger'
      },
      {
        text: 'Cancel',
        actions: onCancelDelete,
        float: 'right',
        color: 'secondary'
      }
    ];

    const dialogText = currentProject ? this.renderProjectCard(currentProject, '', false) : null;

    return (<React.Fragment>
      <div className="row">
        {this.renderProjectCard(currentProject)}
        {this.renderInputBlock(currentProject)}
        {currentProject.exist && currentProject.exist['inp'] && this.renderOutputBlock(currentProject)}
      </div>
      <Modal name="DeleteProjectConfirmation" headerText="Are you sure you want to delete this project?"><Dialog buttons={deleteProjectButtons} dialogText={dialogText} /></Modal>

    </React.Fragment>);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Project);