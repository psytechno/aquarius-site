import React from 'react';
import { connect } from 'react-redux';
import { getProject, setCurrentProject, deleteProject, clearProjectResult, clearProjectError } from '../../../../redux/reducers/project';
import { showModal, hideModal } from '../../../../redux/reducers/ui';
import { Link } from 'react-router-dom';
import Modal from '../../modal';
import Dialog from '../../dialog';

const mapStateToProps = function (state) {
  return {
    ui: state.ui,
    user: state.user,
    project: state.project
  }
};

const mapDispatchToProps = { showModal, getProject, setCurrentProject, clearProjectResult, clearProjectError };

class ProjectList extends React.Component {
  constructor() {
    super();
    this.renderProjectCard = this.renderProjectCard.bind(this);
  }
  componentDidUpdate(prevProps) {
    if (!prevProps.project.result && this.props.project.result) {
      this.props.getProject();
    }
  }
  componentDidMount() {
    this.props.getProject();
  }
  renderProjectCard(project, divClass = 'col-xl-3 col-md-6 mb-4') {
    let d = new Date(project.insertDate);    
    return (

      <div key={project.id} className={divClass}>
        <div className="card shadow h-100 py-2">
          <div className="card-body">
            <div className="row no-gutters align-items-center">
              <div className="col mr-2">
                <Link to={`project/${project.id}`} className={divClass}>
                  <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">{d.toLocaleString()}</div>
                  <div className="h5 mb-0 font-weight-bold text-gray-800">{project.name}</div>
                </Link>
              </div>              
            </div>
          </div>
        </div>
      </div >
    );
  }
  render() {
    const projects = this.props.project.list || [];

    const currentProjectId = this.props.project.current && this.props.project.current.id;
    const onConfirmDelete = [deleteProject(currentProjectId), hideModal("DeleteProjectConfirmation")];
    const onCancelDelete = [hideModal("DeleteProjectConfirmation")];

    const buttons = [
      {
        text: 'Delete',
        actions: onConfirmDelete,
        float: 'left',
        color: 'danger'
      },
      {
        text: 'Cancel',
        actions: onCancelDelete,
        float: 'right',
        color: 'secondary'
      }
    ];

    const currentProject = (this.props.project.list || []).find(p => p && p.id === currentProjectId);

    const dialogText = currentProject ? this.renderProjectCard(currentProject, '') : null;

    return (<React.Fragment>
      <div className="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 className="h3 mb-0 text-gray-800">Projects <Link className="btn btn-success btn-circle" to="/project/new"><i className="fas fa-plus"></i></Link></h1>
      </div>
      <div className="row">
        {projects.map(p => this.renderProjectCard(p))}
      </div>
      <Modal name="DeleteProjectConfirmation" headerText="Are you sure you want to delete this project?"><Dialog buttons={buttons} dialogText={dialogText} /></Modal>
    </React.Fragment>);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectList);