import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = function (state) {
  return {
    ui: state.ui,
    user: state.user
  }
};

const mapDispatchToProps = {};

class Dashboard extends React.Component {
  constructor() {
    super();
  }
  render() {
    return (<React.Fragment>
      <h1 className="h3 mb-4 text-gray-800">Dashboard</h1>      
      <div className="card mb-4">
        <div className="card-header">
          Statistics
        </div>
        <div className="card-body">
          Computational power usage, storage usage.
        </div>
      </div>
    </React.Fragment>);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);