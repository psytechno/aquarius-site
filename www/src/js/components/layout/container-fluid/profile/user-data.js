import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector, reset } from 'redux-form';
import { userUpdate, userClearError, userClearResult } from '../../../../redux/reducers/user';
import { getErrorMessage } from '../../../../stuff';

const formName = 'profile-user-data';

const selector = formValueSelector(formName);

const mapStateToProps = function (state) {
  return {
    user: state.user,
    name: selector(state, 'name')
  }
};

const mapDispatchToProps = { reset, userUpdate, userClearError, userClearResult };

class UserData extends React.Component {
  constructor() {
    super();
  }
  componentDidMount() {
    this.props.userClearError();
    this.props.userClearResult();
    this.props.initialize({
      email: this.props.user.email,
      name: this.props.user.name
    });
  }
  render() {

    let dataChanged = false;
    if (this.props.user.name != this.props.name) {
      dataChanged = true;
    }

    const onSubmit = e => {
      e.preventDefault();
      this.props.userClearError();
      this.props.userClearResult();
      this.props.userUpdate({ id: this.props.user.id, name: this.props.name });
    }

    const onReset = e => {
      this.props.reset(formName);
    }

    const message = getErrorMessage(this.props.user);

    return (
      <div className="card mb-4 new-project">
        <div className="card-header">
          <h4>User profile information</h4>
        </div>
        <div className="card-body">
          <form onSubmit={onSubmit}>
            <div className="form-group">
              <div><span>Email</span></div>
              <div><Field name="email" component="input" type="text" className="form-control" style={{ width: "100%" }} disabled /></div>
            </div>
            <div className="form-group">
              <div><span>Name</span></div>
              <div><Field name="name" component="input" type="text" className="form-control" style={{ width: "100%" }} /></div>
            </div>
            {message &&
              <div className="form-group alert alert-danger">
                {message}
              </div>}
            {this.props.user.result && !dataChanged &&
              <div className="form-group alert alert-success">
                Changes saved
              </div>}
            {dataChanged &&
              <div className="form-group">
                <button className="btn btn-success btn-icon-split" type="submit">
                  <span className="icon text-white-50">
                    <i className="fas fa-check"></i>
                  </span>
                  <span className="text">Save</span>
                </button>
                <button className="btn btn-secondary btn-icon-split" type="reset" style={{ float: "right" }} onClick={onReset}>
                  <span className="icon text-white-50">
                    <i className="fas fa-times"></i>
                  </span>
                  <span className="text">Cancel</span>
                </button>
              </div>}
          </form>
        </div>
      </div>
    );
  }
}

export default reduxForm({ form: formName })(connect(mapStateToProps, mapDispatchToProps)(UserData));