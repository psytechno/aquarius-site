import React from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm, formValueSelector, reset } from 'redux-form';
import { userChangePassword, userClearPasswordError, userClearPasswordResult } from '../../../../redux/reducers/user';
import { getErrorMessage } from '../../../../stuff';

const formName = 'profile-change-password';

const selector = formValueSelector(formName);

const mapStateToProps = function (state) {
  return {
    user: state.user,
    oldPassword: selector(state, 'oldPassword'),
    newPassword: selector(state, 'newPassword'),
    newPassword2: selector(state, 'newPassword2')
  }
};

const mapDispatchToProps = { reset, userChangePassword, userClearPasswordError, userClearPasswordResult };

class ChangePassword extends React.Component {
  constructor() {
    super();
  }
  componentWillMount() {
    this.props.code && this.props.initialize({ oldPassword: this.props.code });
  }
  componentDidMount() {
    this.props.userClearPasswordError();
    this.props.userClearPasswordResult();
  }
  componentDidUpdate(prevProps) {
    if (!prevProps.user.passwordResult && this.props.user.passwordResult) {
      this.props.reset(formName);
    }
  }
  render() {
    let dataChanged = false;
    if (this.props.oldPassword && this.props.newPassword && this.props.newPassword === this.props.newPassword2) {
      dataChanged = true;
    }

    const onSubmit = e => {
      e.preventDefault();
      this.props.userClearPasswordError();
      this.props.userClearPasswordResult();
      this.props.userChangePassword({
        id: this.props.user.id,
        oldPassword: this.props.oldPassword,
        newPassword: this.props.newPassword
      });
    }

    const onReset = e => {
      this.props.reset(formName);
    }

    const message = getErrorMessage(this.props.user, 'passwordError');

    return (
      <div className="card mb-4 new-project">
        <div className="card-header">
          <h4>Change password</h4>
        </div>
        <div className="card-body">
          <form onSubmit={onSubmit}>
            <div className="form-group">
              <div><span>{this.props.code ? "Password reset code" : "Old password"}</span></div>
              <div><Field name="oldPassword" component="input" type={this.props.code ? "text" : "password"} className="form-control" style={{ width: "100%" }} required /></div>
            </div>
            <div className="form-group">
              <div><span>New password</span></div>
              <div><Field name="newPassword" component="input" type="password" className="form-control" style={{ width: "100%" }} required /></div>
            </div>
            <div className="form-group">
              <div><span>Repeat new password</span></div>
              <div><Field name="newPassword2" component="input" type="password" className="form-control" style={{ width: "100%" }} required /></div>
            </div>
            {message &&
              <div className="form-group alert alert-danger">
                {message}
              </div>}
            {this.props.user.passwordResult && !dataChanged &&
              <div className="form-group alert alert-success">
                Password changed
              </div>}
            {dataChanged &&
              <div className="form-group">
                <button className="btn btn-warning btn-icon-split" type="submit">
                  <span className="icon text-white-50">
                    <i className="fas fa-check"></i>
                  </span>
                  <span className="text">Change password</span>
                </button>
                <button className="btn btn-secondary btn-icon-split" type="reset" style={{ float: "right" }} onClick={onReset}>
                  <span className="icon text-white-50">
                    <i className="fas fa-times"></i>
                  </span>
                  <span className="text">Cancel</span>
                </button>
              </div>}
          </form>
        </div>
      </div>
    );
  }
}

export default reduxForm({ form: formName })(connect(mapStateToProps, mapDispatchToProps)(ChangePassword));