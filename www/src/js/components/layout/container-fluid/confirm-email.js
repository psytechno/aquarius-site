
import React from 'react';
import { connect } from 'react-redux';
import { confirmEmail } from '../../../redux/reducers/ui';
import { withRouter } from 'react-router';

const mapStateToProps = function (state) {
    return {
        ui: state.ui
    }
};

const mapDispatchToProps = { confirmEmail };

class ConfirmEmail extends React.Component {
    constructor() {
        super();
    }
    componentDidMount() {
        this.props.confirmEmail(this.props.match.params.code);
    }
    render() {
        return (<React.Fragment>
            <div className="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 className="h3 mb-0 text-gray-800">Account confirmation</h1>
            </div>
            <div className="row">
            { this.props.ui.confirmationError && <div class="form-group alert alert-danger">Wrong or obsolete code</div>}
            { this.props.ui.confirmed && <div class="form-group alert alert-success">Successfully confirmed</div>}
            </div>
        </React.Fragment>);
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ConfirmEmail));