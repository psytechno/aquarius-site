import React from 'react';
import { connect } from 'react-redux';

const mapStateToProps = function (state) {
    return {
      ui: state.ui,
      user: state.user      
    }
  };
  
const mapDispatchToProps = {  };

class Docs extends React.Component {
    constructor() {
      super();
    }
    render() {
      return (<React.Fragment>        
        <div className="card mb-4">
          <div className="card-header">
            <h4>Quick start</h4>
          </div>
          <div className="card-body">
            Ste-by-step example with sample datasets.
          </div>
        </div>
        <div className="card mb-4">
          <div className="card-header">
            <h4>User guide</h4>
          </div>
          <div className="card-body">
            <ul>
              <li>Preparing input data</li>
              <li>Understanding output data</li>
              <li>Data visualisation</li>
            </ul>
          </div>
        </div>
        <div className="card mb-4">
          <div className="card-header">
            <h4>Theory</h4>
          </div>
          <div className="card-body">
            Mathematics and algorithms descriptions
          </div>
        </div>        
      </React.Fragment>);
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Docs);