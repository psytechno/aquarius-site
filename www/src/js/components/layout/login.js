import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userLogin, userCreate, userSetError, userClearError, userClearResult, userResetPassword } from '../../redux/reducers/user';
import { setLoginFormMode } from '../../redux/reducers/ui';
import { Field, reduxForm, formValueSelector } from 'redux-form';
import { getErrorMessage } from '../../stuff';
import { withRouter } from "react-router-dom";

const formName = 'login';

const selector = formValueSelector(formName);

const mapStateToProps = function (state) {
  return {
    ui: state.ui,
    user: state.user,
    email: selector(state, 'email'),
    password: selector(state, 'password'),
    password2: selector(state, 'password2'),
    rememberMe: selector(state, 'rememberMe'),
    acceptTerms: selector(state, 'acceptTerms')
  }
};

const mapDispatchToProps = { userLogin, userCreate, userSetError, userClearError, userClearResult, setLoginFormMode, userResetPassword };

class Login extends React.Component {
  constructor() {
    super();
  }
  componentWillMount() {
    this.props.initialize({ rememberMe: true });
  }
  componentDidMount() {
    this.props.userClearError();
    this.props.userClearResult();
  }
  componentDidUpdate(prevProps) {
    if (this.props.user.id && !prevProps.user.id && this.props.ui.loginFormMode === 'login') {
      this.props.done();
      this.props.history.push("/project");
    }
  }
  getErrorMessage() {
    return getErrorMessage(this.props.user);
  }
  renderGoogle() {
    return (<a href="#" className="btn btn-google btn-block">
      <i className="fab fa-google fa-fw"></i> Login with Google
        </a>);
  }
  renderFacebook() {
    return (<a href="#" className="btn btn-facebook btn-block">
      <i className="fab fa-facebook-f fa-fw"></i> Login with Facebook
        </a>);
  }
  renderLogin() {
    const submitDisabled = () => {
      const { email, password } = this.props;
      if (!email || !password) return true;
      return false;
    }
    const onLogin = e => {
      e.preventDefault();
      const { email, password, rememberMe } = this.props;
      const u = { email, password };
      if (rememberMe)
        u.expiresIn = '10000h';
      this.props.userLogin(u);
    };
    const onCreateNew = () => {
      this.props.setLoginFormMode('register');
      this.props.userClearError();
    };
    const onResetPassword = () => {
      this.props.setLoginFormMode('reset');
      this.props.userClearError();
    };
    const message = this.getErrorMessage();

    return <form name="form-login" className="user" onSubmit={e => onLogin(e)}>
      <div className="form-group">
        <Field autoComplete="email" name="email" component="input" type="email" className="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address..." required />
      </div>
      <div className="form-group">
        <Field autoComplete="password" name="password" component="input" type="password" className="form-control" id="exampleInputPassword" placeholder="Password" required />
      </div>
      <div className="form-group">
        <div className="custom-control custom-checkbox small">
          <Field name="rememberMe" component="input" type="checkbox" className="custom-control-input" id="customCheck" />
          <label className="custom-control-label" htmlFor="customCheck">Remember Me</label>
        </div>
      </div>
      <div className="form-group">
        <button className="btn btn-primary" style={{ width: "30%", float: "left" }} onClick={onLogin} type="submit" disabled={submitDisabled()}>Login</button>
        <a href="#" onClick={onCreateNew} style={{ float: "right" }}>Create new account</a>
        <div style={{ clear: "both" }}></div>
      </div>
      {message &&
        <div className="form-group alert alert-danger">
          {message}
          {message === 'Wrong password' ? <a href="#" onClick={onResetPassword} style={{ float: "right" }}>Reset</a> : null}
        </div>}
    </form>;
  }
  renderRegister() {
    const submitDisabled = () => {
      const { email, password, password2, acceptTerms } = this.props;
      if (!email || !password || !password2 || password != password2 || !acceptTerms) return true;
      return false;
    }
    const onLogin = () => {
      this.props.setLoginFormMode('login');
      this.props.userClearError();
    };
    const onRegister = e => {
      e.preventDefault();
      const { email, password, password2, rememberMe, acceptTerms } = this.props;
      if (!acceptTerms) {
        this.props.userSetError('You must accept Terms'); //TODO: constant
        return;
      }
      if (password != password2) {
        this.props.userSetError('Passwords do not match'); //TODO: constant
        return;
      }
      const u = { email, password };
      if (rememberMe)
        u.expiresIn = '10000h';
      this.props.userCreate(u);
    };

    let message = this.getErrorMessage();

    if (message && message.indexOf('ER_DUP_ENTRY') >= 0) message = 'User already exists'; //TODO: constant

    return <form name="form-register" className="user" onSubmit={e => onRegister(e)}>
      <div className="form-group">
        <Field autoComplete="off" name="email" component="input" type="email" className="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address..." required />
      </div>
      <div className="form-group">
        <Field autoComplete="new-password" name="password" component="input" type="password" className="form-control" id="exampleInputPassword" placeholder="Password" required />
      </div>
      <div className="form-group">
        <Field autoComplete="new-password" name="password2" component="input" type="password" className="form-control" id="exampleInputPassword2" placeholder="Repeat Password" required />
      </div>
      <div className="form-group">
        <div className="custom-control custom-checkbox small" style={{ float: "left" }}>
          <Field name="rememberMe" component="input" type="checkbox" className="custom-control-input" id="customCheck" />
          <label className="custom-control-label" htmlFor="customCheck">Remember Me</label>
        </div>
        <div className="custom-control custom-checkbox small" style={{ float: "right" }}>
          <Field name="acceptTerms" component="input" type="checkbox" className="custom-control-input" id="acceptTerms" />
          <label className="custom-control-label" htmlFor="acceptTerms">I accept <Link to="/terms" target="blank">Terms and Conditions</Link></label>
        </div>
        <div style={{ clear: "both" }}></div>
      </div>
      <div className="form-group">

        <button className="btn btn-primary" style={{ width: "30%", float: "left" }} type="submit" disabled={submitDisabled()}>Register</button>

        <a href="#" onClick={onLogin} style={{ float: "right" }}>Login using existing account</a>
        <div style={{ clear: "both" }}></div>
      </div>
      {message &&
        <div className="form-group alert alert-danger">
          {message}
        </div>}
    </form>;
  }
  renderSuccess() {
    const onClick = () => {
      this.props.done();
      this.props.history.push("/project");
    }

    return (<div className="user">
      <div className="form-group">
        Account created. Confirmation email sent to {this.props.user.email}. You need to confirm email to make actions on this site.
    </div>
      <div className="form-group">
        <button className="btn btn-success" style={{ width: "30%", float: "right" }} onClick={onClick}>OK</button>
      </div>
    </div>);
  }
  renderPwdResetRequest() {
    const submitDisabled = () => {
      const { email } = this.props;
      if (!email) return true;
      return false;
    }
    const onSubmit = e => {
      e.preventDefault();
      const { email } = this.props;
      const u = { email };
      this.props.userClearError();
      this.props.userClearResult();
      this.props.userResetPassword(u);
    };

    const message = this.getErrorMessage();

    return <form name="form-login" className="user" onSubmit={onSubmit}>
      <div className="form-group">
        <Field autoComplete="email" name="email" component="input" type="email" className="form-control" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address..." required />
      </div>
      {message &&
        <div className="form-group alert alert-danger">
          {message}
        </div>}
      {this.props.user.result &&
        <div className="form-group alert alert-success">
          Password reset email sent.
        </div>}
      <div className="form-group">
        {this.props.user.result ?
          <button className="btn btn-secondary" style={{ float: "left" }} type="submit" onClick={this.props.done}>Close</button> :
          <button className="btn btn-primary" style={{ float: "left" }} type="submit" disabled={submitDisabled()}>Send password reset email</button>}
      </div>
    </form>;
  }
  render() {
    if (this.props.user.id)
      return this.renderSuccess();
    if (this.props.ui.loginFormMode === 'reset')
      return this.renderPwdResetRequest();
    return this.props.ui.loginFormMode === 'login' ? this.renderLogin() : this.renderRegister();
  }
};

export default withRouter(reduxForm({ form: formName })(connect(mapStateToProps, mapDispatchToProps)(Login)));

