import React from 'react';
import { connect } from 'react-redux';
import { ok } from 'assert';


class Dialog extends React.Component {
  constructor() {
    super();
    this.renderButton = this.renderButton.bind(this);
  }
  renderButton(btn, i) {
    const onClick = () => btn.actions.map(this.props.dispatch);    
    return <button key={i} className={`btn btn-${btn.color}`} style={{ width: btn.width || "30%", float: btn.float }} onClick={onClick}>{btn.text}</button>
  }
  render() {

    const { dialogText, primaryText, secondaryText } = this.props;

    let primaryFloat = 'left';
    let secondaryFloat = 'right';
    if (this.props.primaryFloat === 'right') {
      primaryFloat = 'right';
      secondaryFloat = 'left';
    }

    const onPrimary = () => this.props.onPrimary.map(this.props.dispatch);
    const onSecondary = () => this.props.onSecondary.map(this.props.dispatch);

    return (
      <React.Fragment>
        {dialogText &&
        <div>
          {dialogText}
        </div>}
        <div className="modal-body">
          {this.props.buttons.map(this.renderButton)}
          <div style={{ clear: "both" }}></div>
        </div>
      </React.Fragment>
    );
  }
};
Dialog.defaultProps = {
  primaryFloat: 'left',
  dialogText: '', //TODO: constant
  primaryText: 'OK',
  secondaryText: 'Cancel',
  onPrimary: [{ type: 'OK' }],
  onSecondary: [{ type: 'CANCEL' }],
  buttons: [
    {
      text: 'OK',
      float: 'left',
      actions: [{ type: 'OK' }],
      color: 'primary'      
    }
  ]
}


export default connect()(Dialog);

