import React from 'react';
import { Link } from 'react-router-dom';

class SidebarElement extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let header = null;
        const {iClassNames, headerText, headerUrl, items, active, expanded, linkClicked} = this.props;      
        
        const modifiedLinkClicked = e => {
            e.preventDefault();
            (typeof linkClicked == 'function') && linkClicked();
        }

        if (headerUrl) {
            header = (<div onClick={modifiedLinkClicked}><Link className="nav-link" to={headerUrl}>
                <i className={iClassNames}></i>
                <span>{headerText}</span></Link></div>);
        } else {
            header = (<a className={"nav-link" + (expanded ? "" : " collapsed")} href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded={expanded} aria-controls="collapsePages">
                <i className={iClassNames}></i>
                <span>{headerText}</span>
            </a>);
        }

        let dropDown = null;
        if (items && Array.isArray(items) && items.length > 0) {
            dropDown = (<div id="collapsePages" className={"collapse" + (expanded?" show":"")} aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div className="bg-white py-2 collapse-inner rounded">
                    {items.map(item => {
                        if (item.type === "header")
                            return <h6 className="collapse-header">{item.text}</h6>;
                        if (item.type === "divider")
                            return <div className="collapse-divider"></div>;
                        if (item.type === "item" || !item.type)
                            return <Link className={"collapse-item" + (item.active ? " active" : "")} href={item.url}>{item.text}</Link>
                    })}
                </div>
            </div>);
        }

        return (<li className={"nav-item" + (active ? " active": "")}>
            {header}
            {dropDown}
        </li>);
    }
}

export default SidebarElement;