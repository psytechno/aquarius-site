import React from 'react';
import { withRouter } from 'react-router';
import getText from "../../lang";
import { toggleSidebar, setSidebarHideAfterClick } from '../../redux/reducers/ui';
import SidebarElement from './sidebar/sidebarElement';
import { connect } from 'react-redux';

const mapStateToProps = function (state) {
    return {
        ui: state.ui,
        user: state.user
    }
};

const mapDispatchToProps = { toggleSidebar, setSidebarHideAfterClick };

class Sidebar extends React.Component {
    constructor() {
        super();
    }
    componentDidMount() {
        window.addEventListener("resize", this.resize.bind(this));
        this.resize();
    }
    resize() {
        if(window.innerWidth < 768) {
            this.props.setSidebarHideAfterClick(true);
            this.props.toggleSidebar(true);
        } else {
            this.props.setSidebarHideAfterClick(false);
        }
    }    
    linkClicked() {
        if (this.props.ui.sidebar.hideAfterClick)
            this.props.toggleSidebar();
    }
    render() {

        const props = { ...this.props};

        const lang = "en"; // TODO: constant

        const sidebarToggled = props.ui.sidebar && props.ui.sidebar.toggled;

        const linkClicked = this.linkClicked.bind(this);

        return (
            <ul className={"navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" + (sidebarToggled ? " toggled" : "")} id="accordionSidebar">

                {/*<!-- Sidebar - Brand -->*/}
                <a className="sidebar-brand d-flex align-items-center justify-content-center" href="#" onClick={() => props.toggleSidebar()}>
                    <div className="sidebar-brand-icon">
                        <i className="fas fa-water"></i>
                    </div>
                    <div className="sidebar-brand-text mx-3">Aquarius</div>
                </a>

                {/*<!-- Divider -->*/}
                <hr className="sidebar-divider my-0" />

                <SidebarElement headerText={getText("Documentation", lang)} headerUrl={"/docs"} iClassNames={"fas fa-fw fa-id-badge"} active={props.match.path === "/docs"} linkClicked={linkClicked}/>

                {props.user.id ?
                    <React.Fragment>
                        <hr className="sidebar-divider my-0" />
                        <SidebarElement headerText={getText("Profile", lang)} headerUrl={"/profile"} iClassNames={"fas fa-fw fa-user-alt"} active={props.match.path === "/profile"}  linkClicked={linkClicked}/>
                        <hr className="sidebar-divider my-0" />
                        <SidebarElement headerText={getText("Dashboard", lang)} headerUrl={"/dashboard"} iClassNames={"fas fa-fw fa-tachometer-alt"} active={props.match.path === "/dashboard"}  linkClicked={linkClicked}/>
                        <hr className="sidebar-divider my-0" />
                        <SidebarElement headerText={getText("Projects", lang)} headerUrl={"/project"} iClassNames={"fas fa-fw fa-project-diagram"} active={props.match.path === "/project"}  linkClicked={linkClicked}/>
                    </React.Fragment> : null}

                {/*<!-- Divider -->*/}
                <hr className="sidebar-divider d-none d-md-block" />

                {/*<!-- Sidebar Toggler (Sidebar) -->*/}
                <div className="text-center d-none d-md-inline">
                    <button className="rounded-circle border-0" id="sidebarToggle" onClick={() => props.toggleSidebar()}></button>
                </div>

            </ul>
        )
    };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Sidebar));