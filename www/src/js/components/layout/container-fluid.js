import React from 'react';
import { withRouter, Route } from 'react-router';
import Docs from './container-fluid/docs';
import Profile from './container-fluid/profile';
import Dashboard from './container-fluid/dashboard';
import Project from './container-fluid/project/project';
import ProjectList from './container-fluid/project/project-list';
import NewProject from './container-fluid/project/new-project';
import ConfirmEmail from './container-fluid/confirm-email';
import ResetPassword from './container-fluid/reset-password';

const ContainerFluid = props => {
  return (
    <div className="container-fluid">
      <Route path="/docs" component={Docs} />
      <Route path="/profile" component={Profile} />
      <Route path="/dashboard" component={Dashboard} />
      <Route exact path="/project/new" component={NewProject} />
      <Route path="/project/:id" render={
        ({ match }) => {
          //if (/^[A-Za-z0-9]{6}$/.test(match.params.id)) {
          if (Number.isInteger(match.params.id*1)) {
            return <Project match={match}/>;
          }
          return null;
        }
      } />
      <Route exact path="/project" component={ProjectList} />
      <Route exact path="/" component={Docs} />
      <Route path="/confirm-email/:code" component={ConfirmEmail} />
      <Route path="/reset-password/:code" component={ResetPassword} />
    </div>);
};

export default withRouter(ContainerFluid);