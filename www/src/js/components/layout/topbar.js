import React from 'react';
import { connect } from 'react-redux';
import { showModal, hideModal, toggleSidebar, setLoginFormMode } from '../../redux/reducers/ui';
import { userLogout, userClearError } from '../../redux/reducers/user';
import { withRouter } from "react-router-dom";
import Modal from './modal';
import Dialog from './dialog';

const mapStateToProps = function (state) {
  return {
    ui: state.ui,
    user: state.user
  }
};

const mapDispatchToProps = { showModal, hideModal, userLogout, userClearError, toggleSidebar, setLoginFormMode };

class Topbar extends React.Component {
  constructor() {
    super();
  }
  componentDidUpdate(prevProps) {
    if (prevProps.user.id && !this.props.user.id) {
      this.props.history.push("/");
    }
  }
  render() {
    const { props } = this;
    const userId = props.user ? props.user.id : null;

    const sidebarToggled = props.ui.sidebar && props.ui.sidebar.toggled;

    const loginClick = () => {
      props.userClearError();
      props.setLoginFormMode('login');
      props.showModal('Authorization');
    }

    const logoutClick = () => {
      props.showModal('LogoutConfirmation');
    }

    const onConfirmLogout = [userLogout(), hideModal('LogoutConfirmation')];
    const onCancelLogout = [hideModal('LogoutConfirmation')];

    const buttons = [
      {
        text: 'Yes',
        actions: onConfirmLogout,
        float: 'left',
        color: 'primary'
      },
      {
        text: 'No',
        actions: onCancelLogout,
        float: 'right',
        color: 'secondary'
      }
    ];

    //return (<div>{userId ? (<div>Hello</div>) : null}<div className={"login"}><Components.AccountsLoginForm/></div></div>);  
    return (
      <React.Fragment>
        <nav className="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          {/*<!-- Sidebar Toggle (Topbar) -->*/}
          {sidebarToggled &&
            <button id="sidebarToggleTop" className="btn btn-link d-md-none rounded-circle mr-3" onClick={() => props.toggleSidebar()}>
              <i className="fa fa-bars"></i>
            </button>}

          {/*<!-- Topbar Search -->
          <form className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
            <div className="input-group">
              <input type="text" className="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
              <div className="input-group-append">
                <button className="btn btn-primary" type="button">
                  <i className="fas fa-search fa-sm"></i>
                </button>
              </div>
            </div>
          </form>*/}

          {/*<!-- Topbar Navbar -->*/}
          <ul className="navbar-nav ml-auto">

            {/*<!-- Nav Item - Search Dropdown (Visible Only XS) -->*/}
            <li className="nav-item dropdown no-arrow d-sm-none">
              <a className="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i className="fas fa-search fa-fw"></i>
              </a>
              {/*<!-- Dropdown - Messages -->*/}
              <div className="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form className="form-inline mr-auto w-100 navbar-search">
                  <div className="input-group">
                    <input type="text" className="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                    <div className="input-group-append">
                      <button className="btn btn-primary" type="button">
                        <i className="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            {/*<!-- Nav Item - Alerts -->*/}
            

            <div className="topbar-divider d-none d-sm-block"></div>

            {/*<!-- Nav Item - User Information -->*/}
            <li className="nav-item dropdown no-arrow">
              <div className="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {(!userId ? (<button className="btn btn-primary" onClick={loginClick}>Log In</button>) : (<a href="#" className="text-gray-600" onClick={logoutClick}>Log out</a>))}
              </div>

            </li>

          </ul>

        </nav>
        <Modal name="LogoutConfirmation" headerText="Log out?"><Dialog buttons={buttons}/></Modal>
      </React.Fragment>
    )
  };
}
export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Topbar));