const texts = {
    "en": {
        "Documentation": "Documentation",
        "Dashboard": "Dashboard",
        "Profile": "Profile",
        "User": "User",
        "Login": "Login",
        "Register": "Register",
        "Settings": "Settings",
        "Email": "Email",
        "Project": "Project",
        "Projects": "Projects",
        "New project": "New project"
    },
    "ru": {
        "Documentation": "Документация",
        "Dashboard": "Управление",
        "Profile": "Профиль",
        "User": "Пользователь",
        "Login": "Вход",
        "Register": "Регистрация",
        "Settings": "Настройки",
        "Email": "Email",
        "Project": "Project",
        "Projects": "Projects",
        "New project": "New project"
    }
}

const getText = (txtCode, lang) => {
    if (!lang) lang = "en";
    if(texts && texts[lang] && texts[lang][txtCode])
        return texts[lang][txtCode];
    return null;
}

export default getText;