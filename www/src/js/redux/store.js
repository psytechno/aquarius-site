import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers";
import { devToolsEnhancer } from 'redux-devtools-extension';
import { apiMiddleware } from 'redux-api-middleware';

const createStoreWithMiddleware = applyMiddleware(apiMiddleware)(createStore);

let preloadedState = localStorage.getItem('reduxState') ? JSON.parse(localStorage.getItem('reduxState')) : {};

preloadedState = { user: preloadedState.user };

export default createStoreWithMiddleware(rootReducer, preloadedState,
  devToolsEnhancer(
    // Specify custom devTools options
  ));