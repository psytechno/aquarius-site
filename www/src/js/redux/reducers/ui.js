import { RSAA } from 'redux-api-middleware'; // RSAA = '@@redux-api-middleware/RSAA'
import { apiUrl } from '../../stuff';
const SHOW_MODAL = 'SHOW_MODAL';
const HIDE_MODAL = 'HIDE_MODAL';
const SET_MODE_LOGIN = 'SET_MODE_LOGIN';
const SET_MODE_REGISTER = 'SET_MODE_REGISTER';

const initialState = { modal: {}, loginFormMode: 'login', sidebar: { toggled: false, hideAfterClick: false } };

export const setSocket = socket => ({
  type: 'SET_SOCKET',
  payload: socket
})

export const confirmEmail = code => ({
  [RSAA]: {
    endpoint: apiUrl + '/auth/' + code,
    method: 'GET',
    types: ['CONFIRM_EMAIL_REQUEST', 'CONFIRM_EMAIL_SUCCESS', 'CONFIRM_EMAIL_FAILURE'],
  }
});

export const setSidebarHideAfterClick = arg => ({
  type: 'SET_SIDEBAR_HIDE_AFTER_CLICK',
  payload: arg
});

export const toggleSidebar = arg => ({
  type: 'TOGGLE_SIDEBAR',
  payload: arg
});

export const setLoginFormMode = mode => ({
  type: 'SET_LOGIN_FORM_MODE',
  payload: mode
});

export const showModal = name => ({
  type: SHOW_MODAL,
  payload: name
});

export const hideModal = name => ({
  type: HIDE_MODAL,
  payload: name
})

export const uiReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_MODAL: {
        window.state = state;
        window.action = action;
      return { ...state, modal: { ...state.modal, [action.payload]: { ...state.modal[action.payload], show: true} } };
    }
    case HIDE_MODAL: {
      return { ...state, modal: { ...state.modal, [action.payload]: { ...state.modal[action.payload], show: false} } };;
    }
    case 'SET_LOGIN_FORM_MODE': {
      return { ...state, loginFormMode: action.payload }
    }    
    case 'TOGGLE_SIDEBAR': {
      const newToggled = (typeof action.payload == 'boolean') ? action.payload : !state.sidebar.toggled;
      return { ...state, sidebar: { ...state.sidebar, toggled: newToggled }}
    }
    case 'SET_SIDEBAR_HIDE_AFTER_CLICK': {
      return { ...state, sidebar: { ...state.sidebar, hideAfterClick: action.payload}}
    }
    case 'CONFIRM_EMAIL_SUCCESS': {
      return { ...state, confirmed: true, confirmationError: null }
    }
    case 'CONFIRM_EMAIL_FAILURE': {
      return { ...state, confirmed: false, confirmationError: action.payload }
    }
    case 'SET_SOCKET': {
      return { ...state, socket: action.payload }
    }
    default: {
      return state;
    }
  }
};