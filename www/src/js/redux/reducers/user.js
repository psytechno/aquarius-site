import { RSAA } from 'redux-api-middleware'; // RSAA = '@@redux-api-middleware/RSAA'
import { parseJwt, getHeaders, apiUrl } from '../../stuff';

const initialState = { id: null };


export const userResetPassword = data => ({
  [RSAA]: {
    endpoint: apiUrl + '/user/reset',
    method: 'PUT',
    types: ['USER_RESET_PASSWORD_REQUEST', 'USER_RESET_PASSWORD_SUCCESS', 'USER_RESET_PASSWORD_FAILURE'],
    body: JSON.stringify(data),
    headers: getHeaders
  }
});

export const userChangePassword = data => ({
  [RSAA]: {
    endpoint: apiUrl + '/auth',
    method: 'PUT',
    types: ['USER_CHANGE_PASSWORD_REQUEST', 'USER_CHANGE_PASSWORD_SUCCESS', 'USER_CHANGE_PASSWORD_FAILURE'],
    body: JSON.stringify(data),
    headers: getHeaders
  }
});

export const userUpdate = user => ({
  [RSAA]: {
    endpoint: apiUrl + '/user',
    method: 'PUT',
    types: ['USER_UPDATE_REQUEST', 'USER_UPDATE_SUCCESS', 'USER_UPDATE_FAILURE'],
    body: JSON.stringify(user),
    headers: getHeaders
  }
});

export const userMe = () => ({
  [RSAA]: {
    endpoint: apiUrl + '/user/me',
    method: 'GET',
    types: ['USER_ME_REQUEST', 'USER_ME_SUCCESS', 'USER_ME_FAILURE'],
    headers: getHeaders
  }
});

export const userCreate = user => ({
  [RSAA]: {
    endpoint: apiUrl + '/auth/create',
    method: 'POST',
    types: ['USER_CREATE_REQUEST', 'USER_CREATE_SUCCESS', 'USER_CREATE_FAILURE'],
    body: JSON.stringify(user),
    headers: { 'Content-Type': 'application/json' }
  }
});

export const userLogin = user => ({
  [RSAA]: {
    endpoint: apiUrl + '/auth',
    method: 'POST',
    types: ['USER_LOGIN_REQUEST', 'USER_LOGIN_SUCCESS', 'USER_LOGIN_FAILURE'],
    body: JSON.stringify(user),
    headers: { 'Content-Type': 'application/json' }
  }
});

export const userLogout = () => ({
  type: 'USER_LOGOUT'
})

export const userSetError = error => ({
  type: 'USER_SET_ERROR',
  payload: error
});

export const userClearError = () => ({
  type: 'USER_CLEAR_ERROR'
});

export const userClearResult = () => ({
  type: 'USER_CLEAR_RESULT'
});

export const userClearPasswordError = () => ({
  type: 'USER_CLEAR_PASSWORD_ERROR'
});

export const userClearPasswordResult = () => ({
  type: 'USER_CLEAR_PASSWORD_RESULT'
});

const getUserWithToken = token => {
  return { ...parseJwt(token), token };
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'USER_RESET_PASSWORD_SUCCESS': {
      return { ...state, result: action.payload }
    }
    case 'USER_RESET_PASSWORD_FAILURE': {
      return { ...state, error: action.payload }
    }
    case 'USER_CHANGE_PASSWORD_SUCCESS': {
      return { ...state, passwordResult: action.payload }
    }
    case 'USER_CHANGE_PASSWORD_FAILURE': {
      return { ...state, passwordError: action.payload }
    }
    case 'USER_UPDATE_SUCCESS': {
      return { ...state, result: action.payload }
    }
    case 'USER_UPDATE_FAILURE': {
      return { ...state, error: action.payload }
    }
    case 'USER_ME_SUCCESS': {
      return { ...state, name: action.payload.name, role: action.payload.role, confirmed: action.payload.confirmed };
    }
    case 'USER_CREATE_SUCCESS': {
      return (getUserWithToken(action.payload.token));
    }
    case 'USER_CREATE_FAILURE': {
      return { ...state, error: action.payload }
    }
    case 'USER_LOGIN_SUCCESS': {
      return (getUserWithToken(action.payload.token));
    }
    case 'USER_LOGIN_FAILURE': {
      return { ...state, error: action.payload }
    }
    case 'USER_SET_ERROR': {
      return { ...state, error: action.payload }
    }
    case 'USER_CLEAR_ERROR': {
      return { ...state, error: null }
    }
    case 'USER_CLEAR_RESULT': {
      return { ...state, result: null }
    }
    case 'USER_CLEAR_PASSWORD_ERROR': {
      return { ...state, passwordError: null }
    }
    case 'USER_CLEAR_PASSWORD_RESULT': {
      return { ...state, passwordResult: null }
    }
    case 'USER_LOGOUT': {
      return initialState;
    }
    default: {
      return state;
    }
  }
};