import { combineReducers } from "redux";
import { userReducer as user } from "./user";
import { gswReducer } from "./gsw";
import { uiReducer as ui } from "./ui";
import { projectReducer as project } from "./project";
import { reducer as form } from 'redux-form'


export default combineReducers({ form, user, ui, project/*, gswReducer*/ });