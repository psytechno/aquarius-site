import { RSAA } from 'redux-api-middleware'; // RSAA = '@@redux-api-middleware/RSAA'
import { getHeaders, currentTimestamp, apiUrl } from '../../stuff';

const initialState = {};

export const setProjectProperty = proj => ({
  type: 'SET_PROJECT_PROPERTY',
  payload: proj
})

export const runProgram = id => ({
  [RSAA]: {
    endpoint: apiUrl + '/project' + (id ? '/' + id : ''),
    method: 'POST',
    types: [
      {
        type: 'PROJECT_RUN_REQUEST',
        meta: action => ({ payload: { id }, action })
      },
      {
        type: 'PROJECT_RUN_SUCCESS',
        meta: action => ({ id, action }) //(action, state, res) => ({ id, path, action, state, res })
      },
      {
        type: 'PROJECT_RUN_FAILURE',
        meta: action => ({ id, action }) //(action, state, res) => ({ id, path, action, state, res })
      }
    ],
    headers: getHeaders
  }
});

export const ifExist = (id, path) => ({
  [RSAA]: {
    endpoint: apiUrl + '/project' + (id ? '/' + id + '/ifexist' : ''),
    method: 'POST',
    types: [
      {
        type: 'PROJECT_IFEXIST_REQUEST',
        meta: action => ({ payload: { id, path }, action })
      },
      {
        type: 'PROJECT_IFEXIST_SUCCESS',
        meta: action => ({ id, path, action }) //(action, state, res) => ({ id, path, action, state, res })
      },
      {
        type: 'PROJECT_IFEXIST_FAILURE',
        meta: action => ({ id, path, action }) //(action, state, res) => ({ id, path, action, state, res })
      }
    ],
    body: JSON.stringify({ path }),
    headers: getHeaders
  }
});

export const dir = (id, path) => ({
  [RSAA]: {
    endpoint: apiUrl + '/project' + (id ? '/' + id + '/dir' : ''),
    method: 'POST',
    types: [
      {
        type: 'PROJECT_DIR_REQUEST',
        payload: () => ({ id, path })
      },
      'PROJECT_DIR_SUCCESS', 'PROJECT_DIR_FAILURE'],
    body: JSON.stringify({ path }),
    headers: getHeaders
  }
});

export const deleteProject = id => ({
  [RSAA]: {
    endpoint: apiUrl + '/project' + (id ? '/' + id : ''),
    method: 'DELETE',
    types: ['DELETE_PROJECT_REQUEST', 'DELETE_PROJECT_SUCCESS', 'DELETE_PROJECT_FAILURE'],
    headers: getHeaders
  }
});

export const updateProject = payload => ({
  [RSAA]: {
    endpoint: apiUrl + '/project',
    method: 'PUT',
    types: ['UPDATE_PROJECT_REQUEST', 'UPDATE_PROJECT_SUCCESS', 'UPDATE_PROJECT_FAILURE'],
    body: JSON.stringify(payload),
    headers: getHeaders
  }
});

export const createProject = payload => ({
  [RSAA]: {
    endpoint: apiUrl + '/project',
    method: 'POST',
    types: ['CREATE_PROJECT_REQUEST', 'CREATE_PROJECT_SUCCESS', 'CREATE_PROJECT_FAILURE'],
    body: JSON.stringify(payload),
    headers: getHeaders
  }
});

export const getProject = id => ({
  [RSAA]: {
    endpoint: apiUrl + '/project' + (id ? '/' + id : ''),
    method: 'GET',
    types: [
      {
        type: 'GET_PROJECT_REQUEST',
        payload: () => ({ id: id })
      },
      'GET_PROJECT_SUCCESS', 'GET_PROJECT_FAILURE'],
    headers: getHeaders
  }
});

export const setProjectPropertyError = error => ({
  type: 'SET_PROJECT_ERROR',
  payload: error
});

export const clearProjectError = () => ({
  type: 'CLEAR_PROJECT_ERROR'
});

export const clearProjectResult = () => ({
  type: 'CLEAR_PROJECT_RESULT'
});

export const setCurrentProject = p => ({
  type: 'SET_CURRENT_PROJECT',
  payload: p
})

const processForRequest = (state, action) => {
  return {
    ...state,
    request: { ...state.request, [action.meta.action['@@redux-api-middleware/RSAA'].types[0].type]: { at: currentTimestamp(), ...action.meta.payload } },
    success: { ...state.success, [action.meta.action['@@redux-api-middleware/RSAA'].types[1].type]: null },
    failure: { ...state.failure, [action.meta.action['@@redux-api-middleware/RSAA'].types[2].type]: null }
  };
}

const processForSuccess = (state, action) => {
  return {
    ...state,
    request: { ...state.request, [action.meta.action['@@redux-api-middleware/RSAA'].types[0].type]: null },
    success: { ...state.success, [action.meta.action['@@redux-api-middleware/RSAA'].types[1].type]: action.payload },
    failure: { ...state.failure, [action.meta.action['@@redux-api-middleware/RSAA'].types[2].type]: null }
  };
}

const processForFailure = (state, action) => {
  return {
    ...state,
    request: { ...state.request, [action.meta.action['@@redux-api-middleware/RSAA'].types[0].type]: null },
    success: { ...state.success, [action.meta.action['@@redux-api-middleware/RSAA'].types[1].type]: null },
    failure: { ...state.failure, [action.meta.action['@@redux-api-middleware/RSAA'].types[2].type]: action.payload }
  };
}

export const projectReducer = (state = initialState, action) => {

  switch (action.type) {
    case 'SET_PROJECT_PROPERTY': {
      return {
        ...state, fromGateway: {
          ...state.fromGateway, [action.payload.id]: state.fromGateway ? {
            ...state.fromGateway[action.payload.id], ...action.payload
          } : {}
        }
      }
    }
    case 'PROJECT_RUN_REQUEST': {
      return processForRequest(state, action);

    }
    case 'PROJECT_RUN_SUCCESS': {
      return processForSuccess(state, action);
    }
    case 'PROJECT_RUN_FAILURE': {
      return processForFailure(state, action);
    }
    case 'PROJECT_IFEXIST_REQUEST': {
      const newState = processForRequest(state, action);
      const list = newState.list && newState.list.map(p => p.id == action.meta.payload.id ? { ...p, exist: { ...p.exist, [action.meta.payload.path]: null } } : p);
      const current = newState.current && newState.current.id == action.meta.payload.id ? { ...newState.current, exist: { ...newState.current.exist, [action.meta.payload.path]: null } } : newState.current;
      return { ...newState, list, current }
    }
    case 'PROJECT_IFEXIST_FAILURE': {
      const list = state.list && state.list.map(p => p.id == action.meta.id ? { ...p, exist: { ...p.exist, [action.meta.path]: false } } : p);
      const current = state.current && state.current.id == action.meta.id ? { ...state.current, exist: { ...state.current.exist, [action.meta.path]: false } } : state.current;
      return { ...state, list, current, request: { ...state.request, ['PROJECT_IFEXIST_REQUEST']: null } }
    }
    case 'PROJECT_IFEXIST_SUCCESS': {
      const list = state.list && state.list.map(p => p.id == action.meta.id ? { ...p, exist: { ...p.exist, [action.meta.path]: action.payload.code } } : p);
      const current = state.current && state.current.id == action.meta.id ? { ...state.current, exist: { ...state.current.exist, [action.meta.path]: action.payload.code } } : state.current;
      return { ...state, list, current, request: { ...state.request, ['PROJECT_IFEXIST_REQUEST']: null } }
    }
    case 'DELETE_PROJECT_FAILURE': {
      return { ...state, error: action.payload }
    }
    case 'DELETE_PROJECT_SUCCESS': {
      return { ...state, result: action.payload }
    }
    case 'UPDATE_PROJECT_FAILURE': {
      return { ...state, error: action.payload }
    }
    case 'UPDATE_PROJECT_SUCCESS': {
      return { ...state, result: action.payload }
    }
    case 'CREATE_PROJECT_REQUEST': {
      return { ...state, request: { ...state.request, ['CREATE_PROJECT_REQUEST']: { at: currentTimestamp(), ...action.payload } } }
    }
    case 'CREATE_PROJECT_SUCCESS': {
      return { ...state, request: { ...state.request, ['CREATE_PROJECT_REQUEST']: null }, result: action.payload }
    }
    case 'CREATE_PROJECT_FAILURE': {
      return { ...state, request: { ...state.request, ['CREATE_PROJECT_REQUEST']: null }, error: action.payload }
    }
    case 'GET_PROJECT_REQUEST': {
      return { ...state, current: null, request: { ...state.request, ['GET_PROJECT_REQUEST']: { at: currentTimestamp(), ...action.payload } } }
    }
    case 'GET_PROJECT_SUCCESS': {
      return {
        ...state,
        request: { ...state.request, ['GET_PROJECT_REQUEST']: null },
        list: Array.isArray(action.payload) ? action.payload : state.list,
        current: !Array.isArray(action.payload) ? action.payload : state.current
      };
    }
    case 'GET_PROJECT_FAILURE': {
      return { ...state, request: { ...state.request, ['GET_PROJECT_REQUEST']: null }, error: action.payload }
    }
    case 'SET_PROJECT_ERROR': {
      return { ...state, error: action.payload }
    }
    case 'CLEAR_PROJECT_ERROR': {
      return { ...state, error: null }
    }
    case 'CLEAR_PROJECT_RESULT': {
      return { ...state, result: null }
    }
    case 'SET_CURRENT_PROJECT': {
      if (action.payload && state.current && action.payload.id === state.current.id)
        return state;
      const current = state.list && state.list.find(p => p.id === action.payload || p.id == action.payload.id) || action.payload;
      return { ...state, current }
    }
    default: {
      return state;
    }
  }
};

export const existPending = (project, id = null, path = null) => {
  return project.request['PROJECT_IFEXIST_REQUEST'] && (id == null || project.request['PROJECT_IFEXIST_REQUEST'].id == id) && (path == null || project.request['PROJECT_IFEXIST_REQUEST'].path == path);
}

export const createPending = (project, id = null) => {
  return project.request && project.request['CREATE_PROJECT_REQUEST'] && (id == null || project.request['CREATE_PROJECT_REQUEST'].id == id);
}