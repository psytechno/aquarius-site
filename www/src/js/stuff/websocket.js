import socketIOClient from "socket.io-client";
export const getSocket = () => socketIOClient(process.env.WS_HOST);