let b64DecodeUnicode = str =>
  decodeURIComponent(
    Array.prototype.map.call(atob(str), c =>
      '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    ).join(''))

export const parseJwt = token =>
  JSON.parse(
    b64DecodeUnicode(
      token.split('.')[1].replace('-', '+').replace('_', '/')
    )
  )

/*export function parseJwt (token) {
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    return JSON.parse(window.atob(base64));
};*/

export const getHeaders = store => ({ 'Content-Type': 'application/json', 'Authorization': `Bearer ${store.user.token}` });
export const getAuthorizationHeader = store => `Bearer ${store.user.token}`;

export const getErrorMessage = (storePart, fieldName = 'error') => {
  let message = storePart[fieldName];
  if (typeof message === 'string')
    return message;
  message = storePart[fieldName] && storePart[fieldName].response && storePart[fieldName].response.message;
  if (typeof message === 'string')
    return message;
  if (Array.isArray(message) && message.length > 0) {
    return message.map(m => m.constraints && typeof m.constraints === 'object' && m.constraints[Object.keys(m.constraints)[0]]).join(', ');
  }
  return null;
}

export const currentTimestamp = () => Math.floor(Date.now() / 1000);

export const apiUrl = process.env.API_HOST + process.env.API_PREFIX;