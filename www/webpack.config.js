const HtmlWebPackPlugin = require("html-webpack-plugin");
let FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const Dotenv = require('dotenv-webpack');
var path = require('path');

module.exports = {
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.html$/,
        use: [
          {
            loader: "html-loader"
          }
        ]
      },
      { test: /(\.css$)/, loaders: ['style-loader', 'css-loader'] }, 
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' }
    ]
  },
  plugins: [
    new FaviconsWebpackPlugin('./static/water-solid.png'),
    new Dotenv(),
    new HtmlWebPackPlugin({
      template: "./src/index.html",
      filename: "./index.html"
    })
  ],
  devServer: {
    contentBase: [path.resolve(__dirname, 'src/css')],
    historyApiFallback: true
  },
  output: {
    publicPath: '/'
  }
};