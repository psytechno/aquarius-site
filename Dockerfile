FROM node:11

##################################################################################
#zip:
RUN apt-get update -y && apt-get install zip -y

##################################################################################
#node.js section:
RUN mkdir /aquarius-site
ADD . /aquarius-site
WORKDIR /aquarius-site
RUN npm i
RUN cd www && npm i && cd ..
EXPOSE 3000
#CMD ["npm", "start"]

RUN mkdir ~/.ssh && ln -s /run/secrets/host_ssh_key ~/.ssh/id_rsa && ln -s /run/secrets/host_ssh_key_pub ~/.ssh/id_rsa.pub

RUN mkdir /aquarius-site/data
RUN mkdir /aquarius-site/data/project

RUN apt-get install sshfs -y

#RUN apt-get install sshfs
