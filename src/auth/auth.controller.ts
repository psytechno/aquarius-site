import { Controller, Get, Post, Put, UseGuards, Body, InternalServerErrorException, Param, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { JwtConfirmedGuard } from './guards/jwt-confirmed.guard';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { AuthUserDto } from '../user/dto/auth-user.dto';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { UpdateUserDto } from '../user/dto/update-user.dto';
import { Usr } from '../user/user.decorator';
import { User } from '../user/interfaces/user.interface';

@Controller('api/auth')
export class AuthController {
  constructor(private readonly authService: AuthService, private readonly userService: UserService) { }

    // TODO: captcha
  @Post('create')
  async create(@Body() createUserDto: CreateUserDto): Promise<any> {
    try {
      const insertResult = await this.userService.create(createUserDto);
      const token = await this.authService.signIn({ ...createUserDto, expiresIn: null });
      return new Promise((resolve, reject) => token ? resolve({ ...insertResult, token }) : reject("no token"));
    } catch (ex) {
      throw new InternalServerErrorException(ex);
    }
  }

    // TODO: captcha
  @Post()
  async signIn(@Body() authUserDto: AuthUserDto): Promise<any> {
    const token = await this.authService.signIn(authUserDto);
    return new Promise((resolve, reject) => token ? resolve({ token }) : reject());
  }

  @Put()
  async changePassword(@Usr() user: User, @Body() updateUserDto: UpdateUserDto): Promise<any> {  
    if (!updateUserDto.newPassword || updateUserDto.newPassword.length < 8)
    throw new BadRequestException('Invalid new password (insufficient length)');
    
    const u = await this.userService.findOneByConfirmationCode(updateUserDto.oldPassword);

    if (u) {
      return this.userService.changePassword({id: u.id, newPassword: updateUserDto.newPassword });
    }    

    if(!user)
      throw new UnauthorizedException('You must be logged in');

    try {
      const token = await this.authService.signIn({email: user.email, password: updateUserDto.oldPassword, expiresIn: '1h'});
    }
    catch (ex) {
      throw new UnauthorizedException('Wrong old password');
    }
    
    return this.userService.changePassword(updateUserDto);
  }

  @Get(':code')
  async findOne(@Param('code') code): Promise<any> {
    const user = await this.userService.findOne({ confirmationCode: code, confirmed: false });
    if (!user)
      throw new UnauthorizedException('Bad code');
    const updateResult = this.userService.confirm(user.id);
    const token = this.authService.getToken(user);
    return new Promise((resolve, reject) => token ? resolve({ ...updateResult, token }) : reject());
  }
}