import {
    Injectable,
    UnauthorizedException,
} from '@nestjs/common';
import { JwtAuthGuard } from './jwt-auth.guard';

@Injectable()
export class JwtAdminGuard extends JwtAuthGuard {
    handleRequest(err, user, info: Error) {
        if (user.role != 'admin')
            throw new UnauthorizedException("Admin only"); //TODO: constant
        return super.handleRequest(err, user, info);
    }
}