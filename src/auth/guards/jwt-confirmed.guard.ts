import {
    Injectable,
    UnauthorizedException,
} from '@nestjs/common';
import { JwtAuthGuard } from './jwt-auth.guard';

@Injectable()
export class JwtConfirmedGuard extends JwtAuthGuard {
    handleRequest(err, user, info: Error) {
        if (user.confirmed != true)
            throw new UnauthorizedException("Please confirm your email"); //TODO: constant
        return super.handleRequest(err, user, info);
    }
}