import { JwtService } from '@nestjs/jwt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { IJwtPayload } from './interfaces/jwt-payload.interface';
import { AuthUserDto } from '../user/dto/auth-user.dto';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) { }

  async signIn(unauthUser: AuthUserDto): Promise<string> {
    // In the real-world app you shouldn't expose this method publicly
    // instead, return a token once you verify user credentials

    const user = await this.userService.findOneByEmail(unauthUser.email);

    if (!user)
      throw new UnauthorizedException("No such user"); //TODO: constant
    
    if (!bcrypt.compareSync(unauthUser.password, user.pwdHash))
      throw new UnauthorizedException("Wrong password"); //TODO: constant

    const payload: IJwtPayload = { id: user.id, name: user.name, email: user.email, role: user.role };
    return this.jwtService.sign(payload, { expiresIn: unauthUser.expiresIn || '3h' }); //TODO: constant
  }

  getToken(user: any) {
    const payload: IJwtPayload = { id: user.id, name: user.name, email: user.email, role: user.role };
    return this.jwtService.sign(payload, { expiresIn: user.expiresIn || '3h' }); //TODO: constant
  }

  async validateUser(payload: IJwtPayload): Promise<any> {
    return await this.userService.findOneByEmail(payload.email);
  }
}
