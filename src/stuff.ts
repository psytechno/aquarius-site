
const { spawn } = require('child_process');

export const spawnHelper = (command, paramsArray, cb) => {
  let stdoutData='', stderrData='';
  const s = spawn(command, paramsArray);
  s.stdout.on('data', data => stdoutData += data);
  s.stderr.on('data', data => stderrData += data);
  s.on('error', data => cb({ event: 'error', data, stdoutData, stderrData}));
  s.on('close', data => cb({ event: 'close', data, stdoutData, stderrData}));
  return s;
}

export const spawnHelperPromise = (command, paramsArray) => new Promise<any>(resolve => spawnHelper(command, paramsArray, resolve));

export const makeid = length => {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}