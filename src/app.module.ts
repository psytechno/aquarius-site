import { Module, MiddlewareConsumer } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProjectModule } from './project/project.module';
import { ProjectService } from './project/project.service';
import { TypeOrmModule, TypeOrmModuleOptions } from '@nestjs/typeorm';
import { Connection } from 'typeorm';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { FrontendMiddleware } from './frontend.middleware';
import { HandlebarsAdapter, MailerModule } from '@nest-modules/mailer';
import { UploadModule } from './upload/upload.module';

@Module({
  imports: [    
    ProjectModule,
    TypeOrmModule.forRootAsync({
      useFactory: () => {
        let options = <TypeOrmModuleOptions>{
          type: 'mysql',
          host: process.env.MYSQL_HOST,
          port: parseInt(process.env.MYSQL_PORT),
          username: process.env.MYSQL_USERNAME,
          password: process.env.MYSQL_PASSWORD,
          database: process.env.MYSQL_DATABASE,
          entities: ["src/**/*.entity{.ts,.js}"],
          synchronize: true
        };
        return options;
      }
    }),
    UserModule,
    AuthModule,
    MailerModule.forRootAsync({
      useFactory: () => {
        return {
          transport: {
            host: process.env.MAILER_HOST,
            port: process.env.MAILER_PORT,
            secure: false, // upgrade later with STARTTLS
            auth: {
              user: process.env.MAILER_USER,
              pass: process.env.MAILER_PASS
            }
          },
          defaults: {
            from: process.env.MAILER_FROM,
          },
          template: {
            dir: __dirname + '/templates',
            adapter: new HandlebarsAdapter(), // or new PugAdapter()
            options: {
              strict: true,
            },
          },
        };
      },
    }),
    UploadModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private readonly connection: Connection) { }
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(FrontendMiddleware)
      .forRoutes('project*');
    consumer
      .apply(FrontendMiddleware)
      .forRoutes('docs*');
    consumer
      .apply(FrontendMiddleware)
      .forRoutes('dashboard*');
    consumer
      .apply(FrontendMiddleware)
      .forRoutes('confirm-email*');
    consumer
      .apply(FrontendMiddleware)
      .forRoutes('reset-password*');    
  }
}
