import { Controller, UseGuards, UseInterceptors, Post, Param, ParseIntPipe, UploadedFile, BadRequestException } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/guards/jwt-auth.guard';
import { SshGuard } from './guards/ssh.guard';
import { JwtConfirmedGuard } from 'src/auth/guards/jwt-confirmed.guard';
import { FileInterceptor } from '@nestjs/platform-express';
import { Usr } from 'src/user/user.decorator';
import { User } from '../user/interfaces/user.interface';
import { ProjectService } from '../project/project.service';
import { Project } from '../project/project.decorator';

@Controller('api/upload')
export class UploadController {
  constructor(private readonly projectService: ProjectService) { }
  @UseGuards(JwtAuthGuard, JwtConfirmedGuard, SshGuard)
  @UseInterceptors(FileInterceptor('file'))
  @Post('project/:id')
  async upload(@Usr() user: User, @Param('id', new ParseIntPipe()) id, @UploadedFile() file, @Project() project): Promise<any> {
    this.projectService.unmount(id, () => {
      if (file && file.originalname && file.originalname.indexOf('.zip') >= 0) {
        this.projectService.unzip(
          project,
          file.originalname,
          (err, res) => console.log(err, res));
      }
    });
    return file;
  }
}
