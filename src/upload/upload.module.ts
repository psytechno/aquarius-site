import { Module } from '@nestjs/common';
import { UploadController } from './upload.controller';
import { MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { ProjectModule } from '../project/project.module';
import { ProjectService } from '../project/project.service';
import { UploadService } from './upload.service';
import { MulterConfigService } from './multer.service';

@Module({
  imports: [
    ProjectModule,
    MulterModule.registerAsync({
      useClass: MulterConfigService
    })
  ],
  controllers: [UploadController],
  providers: [
    // ProjectService,
    UploadService,
    // MulterConfigService
  ],
  exports: [UploadService]
})
export class UploadModule { }
