import { Injectable, CanActivate, ExecutionContext, NotFoundException } from '@nestjs/common';
import { ProjectService } from '../../project/project.service';

@Injectable()
export class SshGuard implements CanActivate {
  constructor(
    private readonly projectService: ProjectService
  ) { }
  async canActivate(
    context: ExecutionContext,
  ): Promise<boolean> {
    const [request, response] = [
      context.switchToHttp().getRequest(),
      context.switchToHttp().getResponse()
    ];

    if (request.route.path.indexOf('project/:id') >= 0) {
      const id = request.params.id;
      const p = await this.projectService.findOne(id);

      if (!p)
        throw new NotFoundException("Project not found");

      if (p.userId != request["user"].id && request["user"].role != 'admin')
        return false;

      const worker = await this.projectService.getWorker(p.workerId);

      if (!worker)
        throw new NotFoundException("Worker not found");

      request["project"] = p;
      request["project"]["worker"] = worker;

      /*let sshfs = () => {
        try {
          this.projectService.sshfs(request["project"]);
          return true;
        }
        catch (e) {
          this.projectService.createFolders(request["project"]);
          try {
            this.projectService.sshfs(request["project"]);
            return true;
          } catch (e2) {
            return false;
          }
        }
      };*/
      let stderrData = (await this.projectService.spawn('ls', [`data/project/${p.id}`])).stderrData      
      if (stderrData) {
        this.projectService.mkdir(p);
      }

      stderrData = (await this.projectService.sshspawn(p, 'dir', [`${worker.folder}\\data\\project\\${p.id}`])).stderrData
      if (stderrData) {
        this.projectService.sshmkdir(p);
        this.projectService.copyProgram(p);
      }

      let sshfs = () => this.projectService.sshfs(request["project"]);

      request["project"]["worker"]["sshfs"] = () => {
        const res = sshfs();
        return res;
      }
    }

    return true;
  }
}