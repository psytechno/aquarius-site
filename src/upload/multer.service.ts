import { Injectable} from '@nestjs/common';
import { MulterModuleOptions, MulterOptionsFactory } from '@nestjs/platform-express';
import * as multer from 'multer';

@Injectable()
export class MulterConfigService implements MulterOptionsFactory {
  createMulterOptions(): MulterModuleOptions {
    return {
      storage: multer.diskStorage({
        destination: async (req, file, cb) => {
          if(req.project && req.project.id) {
            const { id, worker } = req.project;
            if (worker.sshfs())
              cb(null, `${__dirname}/../../data/project/${id}`)
            else
              cb("MulterConfigService: Cannot establish SSHFS connection");
          }
        },
        filename: (req, file, cb) => {
          cb(null, file.originalname);
        },
      }),
      limits: {
        fileSize: 100000000,
      },
      fileFilter: (req, file, cb) => {
        if (file.mimetype != 'application/zip')
          cb(null, false);
        return cb(null, true);        
      }
    };
  }
}