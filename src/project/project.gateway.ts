import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { ProjectService } from './project.service';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Client, Server } from 'socket.io';

@WebSocketGateway(null, { transports: ['websocket'] })
export class ProjectGateway {
  constructor(
    private readonly projectService: ProjectService
  ) { }
  @WebSocketServer()
  server: Server;

  private readonly clients = {};  

  async handleConnection(data){

    // A client has connected

    // Notify connected clients of current users
    this.server.emit('hello', 'server connected');

}

async handleDisconnect(data){ 
  this.projectService.removeClient(data.client);
}

  @SubscribeMessage('events')
  findAll(client: Client, data: any): Observable<WsResponse<number>> {
    return from([1, 2, 3]).pipe(map(item => ({ event: 'events', data: item })));
  }

  @SubscribeMessage('identity')
  async identity(client: Client, data: number): Promise<number> {
    return data;
  }

  @SubscribeMessage('project')
  async project(client: Client, data: any): Promise<any> {
    if (data.id && data.tempId) {
      if (this.projectService.checkTempId(data.id, data.tempId)) {
        this.clients[client.id] = client;
        this.projectService.addClient(data.id, client);
      }
    }
  }
}