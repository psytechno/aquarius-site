import {
  Controller,
  HttpCode,
  Query,
  Res,
  Get,
  Post,
  Put,
  Body,
  Param,
  ParseIntPipe,
  Delete,
  UseGuards,
  UnauthorizedException,
  NotFoundException,
  InternalServerErrorException,
  BadRequestException,
} from '@nestjs/common';
import { CreateProjectDto } from './dto/create-project.dto';
import { UpdateProjectDto } from './dto/update-project.dto';
import { ProjectService } from './project.service';
import { IProject } from './interfaces/project.interface';
import { DeleteResult, InsertResult, UpdateResult } from 'typeorm';
import { Usr } from '../user/user.decorator';
import { Project } from './project.decorator';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { JwtConfirmedGuard } from '../auth/guards/jwt-confirmed.guard';
import { User } from '../user/interfaces/user.interface';
import { SshGuard } from '../upload/guards/ssh.guard';
import { get } from 'https';
var path = require('path');
const fs = require('fs');
import { makeid } from '../stuff';

@Controller('api/project')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) { }
  @UseGuards(JwtAuthGuard, JwtConfirmedGuard)
  @Post()
  async create(@Usr() user: User, @Body() createProjectDto: CreateProjectDto): Promise<InsertResult> {
    return this.projectService.create({ ...createProjectDto, userId: user.id, id: null, insertDate: null, workerId: 1 });
  }

  @UseGuards(JwtAuthGuard, JwtConfirmedGuard, SshGuard)
  @Post(':id')
  async runProgram(@Project() project, @Param('id', new ParseIntPipe()) id): Promise<any> {
    const runProgram = new Promise((resolve, reject) => this.projectService.runProgram(project, (err, res) => {
      this.projectService.zipOut(project);
      return err ? reject(err) : resolve(res);
    }));
    const wait = new Promise(resolve => setTimeout(() => resolve({ status: "Process started" }), 500));
    return Promise.race([runProgram, wait]);
  }

  @UseGuards(JwtAuthGuard, JwtConfirmedGuard)
  @Put()
  async update(@Usr() user: User, @Body() updateProjectDto: UpdateProjectDto): Promise<UpdateResult> {
    const p = await this.projectService.findOne(updateProjectDto.id);
    if (!p)
      throw new NotFoundException("No such project");
    if (p.userId != user.id)
      throw new UnauthorizedException("Access denied - wrong user");
    return this.projectService.update(updateProjectDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  async findAll(@Usr() user: User): Promise<IProject[]> {
    return this.projectService.findByUserId(user.id);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@Usr() user: User, @Param('id', new ParseIntPipe()) id): Promise<any> {
    const p = await this.projectService.findOne(id);

    if (!p)
      throw new NotFoundException();
    if (p.userId != user.id && user.role != 'admin')
      throw new UnauthorizedException("Access denied");

    const tempId = makeid(32);
    this.projectService.setTempId(id, tempId);

    return new Promise<any>(resolve => resolve({ ...p, tempId }));
  }

  @UseGuards(JwtAuthGuard, JwtConfirmedGuard, SshGuard)
  @HttpCode(200)
  @Post(':id/dir')
  async checkInput(@Project() project, @Body() body) {
    if (body.path.indexOf('..') >= 0)
      throw new BadRequestException("Please specify relative path inside your project folder");
    const p = new Promise(resolve => {
      this.projectService.dir(project, body.path, (err, res) => {
        if (err)
          resolve();
        else
          resolve(res);
      })
    });
    const res = await p;
    if (res) return { res };
    throw new NotFoundException("Path not found");
  }

  @UseGuards(JwtAuthGuard, JwtConfirmedGuard, SshGuard)
  @HttpCode(200)
  @Post(':id/ifexist')
  async checkResult(@Project() project, @Body() body) {
    if (body.path.indexOf('..') >= 0)
      throw new BadRequestException("Please specify relative path inside your project folder");
    const p = new Promise(resolve => {
      this.projectService.ifexist(project, body.path, (err, res) => {
        if (err)
          resolve();
        else
          resolve(res);
      })
    });
    const res = await p;
    if (res) {
      return { code: res['code'] };
    }
    throw new NotFoundException("Path not found");
  }

  @UseGuards(JwtAuthGuard, JwtConfirmedGuard, SshGuard)
  @Get(':id/out')
  async getResult(@Param('id', new ParseIntPipe()) id, @Project() project, @Res() res) {
    if (project.worker.sshfs()) {
      const outName = path.join(__dirname, '../../data/project/' + id, 'OUT.zip');
      try {
        if (fs.existsSync(outName)) {
          res.sendFile(outName);
        } else {
          throw new NotFoundException("OUT.zip not found");
        }
      } catch (ex) {
        throw new NotFoundException("Unable to send OUT.zip");
      } finally {
        this.projectService.unmount(id);
      }
    } else {
      throw new InternalServerErrorException("Cannot establish SSHFS connection");
    }
  }

  @Get('download/:code')
  async downloadFile(@Param('code') code, @Res() res) {
    const f = this.projectService.getFileMetaObj(code);

    if (!f)
      throw new NotFoundException("Invalid code");

    if (f.project.worker.sshfs()) {
      const outName = path.join(__dirname, '../../data/project/' + f.project.id, f.path);
      try {
        if (fs.existsSync(outName)) {
          res.sendFile(outName);
        } else {
          throw new NotFoundException(`${f.path} not found`);
        }
      } catch (ex) {
        throw new NotFoundException(`Unable to send ${f.path}`);
      } finally {
        this.projectService.unmount(f.project.id);
      }
    } else {
      throw new InternalServerErrorException("Cannot establish SSHFS connection");
    }

  }

  @UseGuards(JwtAuthGuard, JwtConfirmedGuard)
  @Delete(':id')
  async deleteOne(@Usr() user: User, @Param('id', new ParseIntPipe()) id): Promise<DeleteResult> {
    const p = await this.projectService.findOne(id);
    if (!p)
      throw new NotFoundException();
    if (p.userId == user.id || user.role == 'admin')
      return this.projectService.deleteOne(id);
    throw new UnauthorizedException("Cannot delete this");
  }

  @UseGuards(JwtAuthGuard, JwtConfirmedGuard)
  @Post(':id/spawn')
  async spawn(@Param('id', new ParseIntPipe()) id, @Usr() user: User, @Body() body) {
    if (!body.ssh)
      return await this.projectService.spawn(body.command, body.args);

    const p = await this.projectService.findOne(id);

    if (p.userId != user.id)
      throw new UnauthorizedException(`Not your project (p.userId=${p.userId}, user.id=${user.id})`);

    p.worker = await this.projectService.getWorker(p.workerId);
    return await this.projectService.sshspawn(p, body.command, body.args);
  }

  @UseGuards(JwtAuthGuard, JwtConfirmedGuard)
  @Post(':id/message')
  async message(@Param('id', new ParseIntPipe()) id, @Usr() user: User, @Body() body) {
  }
}
