import { IsInt, IsString, IsDateString, IsNumber } from 'class-validator';

export class GetProjectDto {
    @IsInt()
    readonly id: number;
    @IsInt()
    readonly userId: number;
    @IsString()
    readonly name: string;    
    @IsDateString()
    readonly insertdate: string;
    //readonly parameters: CreateProjectParametersDto;
}