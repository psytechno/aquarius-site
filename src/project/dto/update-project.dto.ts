import { IsInt, IsString, IsDateString, IsNumber } from 'class-validator';

export class UpdateProjectDto {
    @IsInt()
    readonly id: number;
    @IsString()
    readonly name: string;
}