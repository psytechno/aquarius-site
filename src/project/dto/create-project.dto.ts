import { IsInt, IsString, IsDateString, IsNumber } from 'class-validator';

export class CreateStepDto {
    readonly correction: string;
    readonly tangent: string;
    readonly debugIndices: number[];
    readonly limiter: number;
}
export class CreateProjectParametersDto {
    readonly weightForLocalTimeStep: number;
    readonly orderOfScheme: number;
    readonly roughnessCoefficient: number;
    readonly downstreamWaterLevel: number;
    readonly upstreamWaterLevel: number;
    readonly kurant: number;
    readonly rain: number;
    readonly relativeAccuracyOfDepthCalculation: number;
    readonly dryBottomDepth: number;
    readonly intervalForPrinting: number;
    readonly timeScale: string;
    readonly volumeScale: string;
    readonly step: CreateStepDto;
}

export class CreateProjectDto {
    @IsString()
    readonly name: string;    
    //readonly parameters: CreateProjectParametersDto;
}