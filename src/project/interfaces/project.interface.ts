export interface Step {
  correction: string,
  tangent: string,
  debugIndices: number[],
  limiter: number
}
export interface ProjectParameters {
  weightForLocalTimeStep: number,
  orderOfScheme: number,
  roughnessCoefficient: number,
  downstreamWaterLevel: number,
  upstreamWaterLevel: number,
  kurant: number,
  rain: number,
  relativeAccuracyOfDepthCalculation: number,
  dryBottomDepth: number,
  intervalForPrinting: number,
  timeScale: string,
  volumeScale: string,
  step: Step
}
export interface IProject {
  id: number,
  userId: number,
  insertDate: Date,
  name: string,
  workerId: number
}

export interface IUpdateProject {
  id: number,
  name: string
}