import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, OneToMany, JoinTable } from 'typeorm';

@Entity('t_worker')
export class WorkerEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 256 })
  user: string;

  @Column({ length: 256 })
  host: string;

  @Column({ length: 256 })
  folder: string;
}
@Entity('t_project')
export class ProjectEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 256 })
  name: string;

  @Column({ type: 'int', name: 'user_id' })
  userId: number;

  @Column({ type: 'timestamp', name: 'insertdate', default: () => 'CURRENT_TIMESTAMP' })
  insertDate: Date;

  @Column({ type: 'int', name: 'worker_id', default: 1, nullable: true })
  workerId: number; 
}

