import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, InsertResult, DeleteResult, UpdateResult } from 'typeorm';
import { IProject, IUpdateProject } from './interfaces/project.interface';
import { ProjectEntity, WorkerEntity } from './project.entity';
import { makeid, spawnHelperPromise } from '../stuff';
import { identifier } from '@babel/types';

const { exec, execSync } = require('child_process');


@Injectable()
export class ProjectService {
  private readonly projects = {};
  private readonly files = {};

  addClient = (id, client) => {
    console.log('add client');
    const clients = this.projects[id] ? { ...this.projects[id].clients, [client.id]: client } : { [client.id]: client }
    this.setProjectProperty(id, { clients });
  }

  removeClient = (client) => {
    Object.keys(this.projects).map(pid => {
      if (this.projects[pid].clients && this.projects[pid].clients[client.id]) {
        delete this.projects[pid].clients[client.id];
      }
    })
  }

  setProjectProperty = (id, probj, emitMessage = true) => {
    this.projects[id] = { ...this.projects[id], ...probj };
    if (emitMessage && this.projects[id].clients) {
      Object.keys(this.projects[id].clients).map(cid => this.projects[id].clients[cid].emit('setProjectProperty', { id, ...probj }));
    }
  }
  setTempId = (id, tempId) => {
    const tempIds = this.projects[id] ? { ...this.projects[id].tempIds, [tempId]: true } : { [tempId]: true };
    this.setProjectProperty(id, { tempIds });
    setTimeout(() => {
      delete this.projects[id].tempIds[tempId];
    }, 6000);
  }
  checkTempId = (id, tempId) => {
    return this.projects[id] && this.projects[id].tempIds && this.projects[id].tempIds[tempId];
  }
  constructor(
    @InjectRepository(ProjectEntity) private readonly projectRepository: Repository<ProjectEntity>,
    @InjectRepository(WorkerEntity) private readonly workerRepository: Repository<WorkerEntity>) { }

  async create(p: IProject): Promise<any> {
    delete (p.id);
    delete (p.insertDate);

    //ssh p79629759529@35.228.176.120 'mkdir gsw\data\project\2'
    //const ssh_mkdir = spawn('ssh', [process.env.CPU001_USER + '@' + process.env.CPU001_HOST, "'mkdir gsw\\data\\project\\"++"'"]);

    const worker = await this.getWorker(p.workerId);

    if (!worker) {
      throw new NotFoundException("Worker not found");
    }

    const res = await this.projectRepository.insert(p);

    let fsResults = {};

    const prj = {
      id: res.identifiers[0].id,
      worker
    };

    try {
      if (res && res.identifiers && res.identifiers[0] && res.identifiers[0].id) {
        fsResults = this.createFolders(prj);
        fsResults['ssh-copy'] = this.copyProgram(prj);
      }
    } catch (e) {
      fsResults["exception"] = e;
    }
    return { ...res, fsResults };
  }

  async update(p: IUpdateProject): Promise<UpdateResult> {
    return this.projectRepository.update({ id: p.id }, p);
  }

  async findAll(): Promise<ProjectEntity[]> {
    return this.projectRepository.find();
  }
  async findByUserId(userId): Promise<ProjectEntity[]> {
    return this.projectRepository.find({ userId });
  }
  async findOne(projectId): Promise<any> {
    const p = await this.projectRepository.findOne(projectId);

    const addP = {...this.projects[projectId]};

    addP.numberOfClients = addP.clients ? Object.keys(addP.clients).length : 0;

    delete addP.clients;

    return { ...p, ...addP };
  }
  async deleteOne(projectId): Promise<DeleteResult> {
    return this.projectRepository.delete(projectId);
  }

  async getWorker(id): Promise<WorkerEntity> {
    return this.workerRepository.findOne(id);
  }

  copyProgram(project) {
    return execSync(`ssh ${project.worker.user}@${project.worker.host} copy ${project.worker.folder}\\\\bin\\\\*.* ${project.worker.folder}\\\\data\\\\project\\\\${project.id}\\\\*.*`);
  }

  createFolders(project) {
    const ret = {};
    try {
      ret["ssh-mkdir"] = this.sshmkdir(project);
    } catch (ex) {
    }
    try {
      ret["mkdir"] = this.mkdir(project);
    } catch (ex) {
    }
    return ret;
  }
  mkdir(project) {
    return execSync(`mkdir data/project/${project.id}`);
  }
  sshmkdir(project) {
    return execSync(`ssh ${project.worker.user}@${project.worker.host} mkdir ${project.worker.folder}\\\\data\\\\project\\\\${project.id}`);
  }

  sshfs(project) {
    try {
      execSync(`sshfs ${project.worker.user}@${project.worker.host}:${project.worker.folder}\\\\data\\\\project\\\\${project.id} data/project/${project.id}`);
      this.setProjectProperty(project.id, { sshfs: true });
    }
    catch (ex) {
      return false;
    }
    return true;
  }

  unmount(pid, callback = r => { }) {
    const u = id => {
      exec(`fusermount -u data/project/${id}`, (err, res) => {
        if (err) {
          setTimeout(() => u(id), 500);
        } else {
          this.setProjectProperty(id, { sshfs: false, pendingUnmount: false });
          callback && callback(res);
        }
      });
    };
    this.setProjectProperty(pid, { pendingUnmount: true });
    u(pid);
  }

  unzip(project, fileName, callback = (err, res) => { }) {
    this.setProjectProperty(project.id, { unzip: true });
    exec(`ssh ${project.worker.user}@${project.worker.host} powershell.exe -nologo -noprofile -command "Expand-Archive -LiteralPath ${project.worker.folder}\\\\data\\\\project\\\\${project.id}\\\\${fileName} -DestinationPath ${project.worker.folder}\\\\data\\\\project\\\\${project.id}"`,
      (err, res) => {
        this.setProjectProperty(project.id, { unzip: false });
        callback(err, res);
      });
  }
  unzipSync(project, fileName) {
    return execSync(`ssh ${project.worker.user}@${project.worker.host} powershell.exe -nologo -noprofile -command "Expand-Archive -LiteralPath ${project.worker.folder}\\\\data\\\\project\\\\${project.id}\\\\${fileName} -DestinationPath ${project.worker.folder}\\\\data\\\\project\\\\${project.id}"`);
  }
  //Compress-Archive -U -Path C:\Users\Administrator\Desktop\testing\* -DestinationPath C:\Users\Administrator\Desktop\test
  zipOut(project, callback = (err, res) => { }) {
    this.setProjectProperty(project.id, { zipOut: true });
    exec(`ssh ${project.worker.user}@${project.worker.host} powershell.exe -nologo -noprofile -command "Compress-Archive -U -Path ${project.worker.folder}\\\\data\\\\project\\\\${project.id}\\\\OUT\\\\* -DestinationPath ${project.worker.folder}\\\\data\\\\project\\\\${project.id}\\\\OUT"`,
      (err, res) => {
        this.setProjectProperty(project.id, { zipOut: false });
        callback(err, res);
      });
  }

  runProgram(project, callback = (err, res) => { }) {
    this.setProjectProperty(project.id, { runProgram: true });
    exec(`ssh ${project.worker.user}@${project.worker.host} ${project.worker.folder}\\\\data\\\\project\\\\${project.id}\\\\GSW.exe -y`,
      (err, res) => {
        this.setProjectProperty(project.id, { runProgram: false });
        callback(err, res);
      });
  }

  dir(project, path = 'inp', callback = (err, res) => { }) {
    exec(`ssh ${project.worker.user}@${project.worker.host} dir ${project.worker.folder}\\\\data\\\\project\\\\${project.id}\\\\${path}`,
      callback);
  }
  ifexist(project, path = 'OUT.zip', callback = (err, res) => { }) {
    this.setProjectProperty(project.id, { ifexist: path });
    exec(`ssh ${project.worker.user}@${project.worker.host} if exist ${project.worker.folder}\\\\data\\\\project\\\\${project.id}\\\\${path} echo exist`,
      (err, res) => {
        if (res) {
          const code = makeid(32);
          this.files[code] = { project, path };
          this.setProjectProperty(project.id, { ifexist: null });
          callback(null, { ...res, code });
        } else {
          this.setProjectProperty(project.id, { ifexist: null });
          callback(err, res);
        }
      });
  }
  spawn(command, args) {
    return spawnHelperPromise(command, args);
  }
  sshspawn(project, command, args) {
    return spawnHelperPromise(`ssh`, [`${project.worker.user}@${project.worker.host}`, command, ...args]);
  }

  getFileMetaObj(code) {
    return this.files[code];
  }
}
