import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectController } from './project.controller';
import { ProjectService } from './project.service';
import { ProjectEntity, WorkerEntity } from './project.entity';
import { ProjectGateway } from './project.gateway';
@Module({
  imports: [TypeOrmModule.forFeature([ProjectEntity, WorkerEntity])],
  controllers: [ProjectController],
  providers: [ProjectService, ProjectGateway],
  exports: [ProjectService]
})
export class ProjectModule { }
