import { IsString, IsEmail, Length } from 'class-validator';

export class CreateUserDto {
  readonly name: string;

  @Length(6, 256)
  @IsEmail()
  readonly email: string;

  @Length(8, 72)
  @IsString()
  readonly password: string;

  readonly expiresIn: string;
}