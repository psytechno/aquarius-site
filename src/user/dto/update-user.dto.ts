import { IsString, IsInt } from 'class-validator';

export class UpdateUserDto {
  readonly id: number;

  readonly name: string;

  readonly oldPassword: string;

  readonly newPassword: string;

  readonly email: string;
}