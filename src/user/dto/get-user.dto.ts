import { IsString, IsEmail, Length, IsInt, IsBoolean } from 'class-validator';

export class GetUserDto {  
    @IsInt()
    readonly id: number;

    @IsString()
    readonly name: string;    

    @IsEmail()
    readonly email: string;

    @IsString()
    readonly role: string;

    @IsBoolean()
    readonly confirmed: boolean;
}