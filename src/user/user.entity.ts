import { Entity, Column, PrimaryGeneratedColumn, Index } from 'typeorm';

@Entity('t_user')
export class UserEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 256, default: null })
  name: string;

  @Index({ unique: true })
  @Column({ length: 256 })
  email: string;

  @Column({ name: 'pwd_hash', length: 60 })
  pwdHash: string;

  @Column({ length: 32 })
  role: string;

  @Column({ name: 'confirmation_code', length: 32, default: null })
  confirmationCode: string;

  @Column()
  confirmed: boolean;
}