import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, InsertResult, DeleteResult, UpdateResult } from 'typeorm';
import { UserEntity } from './user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import * as bcrypt from 'bcryptjs';
import { MailerService } from '@nest-modules/mailer';
import { makeid } from '../stuff';

@Injectable()
export class UserService {
  private salt = bcrypt.genSaltSync(10);
  constructor(@InjectRepository(UserEntity)
  private readonly userRepository: Repository<UserEntity>,
    private readonly mailerService: MailerService) { }

  async create(createUserDto: CreateUserDto): Promise<InsertResult> {
    const hash = this.getHash(createUserDto.password);
    const confirmationCode = makeid(32);
    let u = <UserEntity>{
      ...createUserDto,
      pwdHash: hash,
      role: 'user',
      id: undefined,
      confirmationCode,
      confirmed: false
    }

    this
        .mailerService
        .sendMail({
          to: createUserDto.email, // sender address
          subject: 'GSW-Online - Registration ✔', // Subject line
          text: 'Welcome! To confirm registration, follow this link ' + process.env.SITE_HOST + '/confirm-email/' + confirmationCode, // plaintext body
          html: '<b>Welcome!</b> To confirm registration, follow this link <a href="' + process.env.SITE_HOST + '/confirm-email/' + confirmationCode + '">' + process.env.SITE_HOST + '/confirm-email/' + confirmationCode + '</a>' // HTML body content
        }).then(console.log).catch(console.error);

    return await this.userRepository.insert(u);
  }

  async resetPassword(updateUserDto: UpdateUserDto): Promise<UpdateResult> {
    const u = await this.userRepository.findOne({email: updateUserDto.email });

    if (!u)
      throw new NotFoundException('No user with this email'); // TODO: constant

    const confirmationCode = makeid(32);

    this
        .mailerService
        .sendMail({
          to: u.email, // sender address
          subject: 'GSW-Online - Password reset ✔', // Subject line
          text: 'Use this link to reset password ' + process.env.SITE_HOST + '/reset-password/' + confirmationCode, // plaintext body
          html: 'Use this link to reset password <a href="' + process.env.SITE_HOST + '/reset-password/' + confirmationCode + '">' + process.env.SITE_HOST + '/reset-password/' + confirmationCode + '</a>' // HTML body content
        }).then(console.log).catch(console.error);

    return this.userRepository.update({id: u.id}, { confirmationCode });
  }

  async confirm(userId): Promise<any> {
    return this.userRepository.update(userId, { confirmed: true });
  }

  async findAll(): Promise<UserEntity[]> {
    return this.userRepository.find();
  }
  async findOne(userId): Promise<UserEntity> {
    return this.userRepository.findOne(userId);
  }
  async findOneByEmail(email): Promise<UserEntity> {
    return this.userRepository.findOne({ email });
  }
  async findOneByConfirmationCode(confirmationCode): Promise<UserEntity> {
    return this.userRepository.findOne({ confirmationCode });
  }
  async deleteOne(userId): Promise<DeleteResult> {
    return this.userRepository.delete(userId);
  }
  getHash(password: string | undefined): string {
    return bcrypt.hashSync(password, this.salt);
  }

  async setName(updateUserDto: UpdateUserDto): Promise<UpdateResult> {
    return await this.userRepository.update({id: updateUserDto.id}, { name: updateUserDto.name});
  }

  async changePassword(updateUserDto: any): Promise<UpdateResult> {
    const hash = this.getHash(updateUserDto.newPassword);
    return await this.userRepository.update({id: updateUserDto.id}, { pwdHash: hash, confirmationCode: null });
  }
}
