
import { Controller, Get, Put, Body, Param, ParseIntPipe, Delete, UseGuards, Req, UnauthorizedException, BadRequestException } from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './interfaces/user.interface';
import { GetUserDto } from './dto/get-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { DeleteResult, UpdateResult } from 'typeorm';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { JwtAdminGuard } from '../auth/guards/jwt-admin.guard';
import { JwtConfirmedGuard } from '../auth/guards/jwt-confirmed.guard';
import { Usr } from './user.decorator';

@Controller('api/user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @UseGuards(JwtAuthGuard, JwtAdminGuard)
  @Get()
  async findAll(@Usr() user: User): Promise<GetUserDto[]> {
    return this.userService.findAll();
  }

  @Get('me')
  @UseGuards(JwtAuthGuard)
  async findMe(@Usr() user: User): Promise<GetUserDto> {
    const userEntity = await this.userService.findOne(user.id);
    const userDto = <GetUserDto>userEntity;
    delete userDto['pwdHash'];
    delete userDto['confirmationCode'];
    return new Promise(resolve => resolve(userDto));
  }

  @UseGuards(JwtAuthGuard, JwtAdminGuard)
  @Get(':id')
  async findOne(@Param('id', new ParseIntPipe()) id): Promise<User> {
    return this.userService.findOne(id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async deleteOne(@Usr() user: User, @Param('id', new ParseIntPipe()) id): Promise<DeleteResult> {
    if (user.role == 'admin')
      return this.userService.deleteOne(id);
  }

  @UseGuards(JwtAuthGuard, JwtConfirmedGuard)
  @Put()
  async update(@Usr() user: User, @Body() updateUserDto: UpdateUserDto): Promise<UpdateResult> {    
    if (user.id != updateUserDto.id)
      throw new UnauthorizedException("Wrong user");
    if (updateUserDto.name)
      return this.userService.setName(updateUserDto);
    else throw new BadRequestException("Name is empty");
  }

  // TODO: captcha
  @Put('reset')
  async reset(@Body() updateUserDto: UpdateUserDto): Promise<UpdateResult> {
    return this.userService.resetPassword(updateUserDto);
  }
}
